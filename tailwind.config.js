module.exports = {
  content: [
    './public/**/*.{html,php,js}',
  ],
  theme: {
    container:{
      center: true,
      padding: '1.75rem',
      screens: {
        sm: "100%",
        md: "100%",
        lg: "100%",
        xl: "1256px"
      }
    },
    extend: {
      fontFamily: {
        'clavo': ['"Clavo"', 'serif'],
        'proxima': ['"Proxima Nova"', 'sans-serif'],
      },
      colors:{
        'med-blue': {
          100 : '#63AEB5',
          200 : '#ADB5B9',
          300 : '#838D93',
          400 : '#8E9396',
        },
        'soft-black': '#232D33',
        'gold': {
          100 : '#D3B960',
          200 : '#BA9D0F',
        },
        'gray': {
          100 : '#44535C',
          200 : '#8E9396',
          300 : '#A1B5B5',
          400 : '#E1E5E5',
          500 : '#D4D4CF',
          600 : '#C9D0D0'
        },
        'red' : {
          50  : '#F568CD',
          100 : '#BF3F27',
        },
      },
      backgroundImage: {
        //'aqua-pattern': "url('./img/background/aqua-bg-pattern.svg')",
      },
      backgroundPosition: {
        //right: 'right',
        //'right-bottom-10-30': 'right 10% bottom 30%',
      }
    },
  },
  plugins: [],
}
