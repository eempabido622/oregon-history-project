<?php include "./header.html"; ?>
<article>
    <section class="page-banner min-h-[16.25rem] md:min-h-[26.25rem] bg-gray-100" style="background-image:url('./img/placeholder/curator-article-banner.jpg');">
        <div class="container justify-center pt-[5.813rem] md:pt-0 md:justify-end">
            <div class="inner max-w-[44.688rem] pb-[1.875rem] md:pb-10 xl:pb-20">
            </div>
        </div>
    </section><!-- End of page-banner-->

    <section class="bg-white pt-[1.875rem] pb-11 md:pt-8 md:pb-[6.563rem]">
        <div class="container pb-[1.875rem] md:pb-[3.688rem]">
            <div class="wrapper flex">
                <div class="left hidden pt-3.5 lg:block basis-full lg:basis-[10.75rem] shrink-0">
                    <a href="#" class="btn-link">
                        <svg class="mr-" xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17" fill="none">
                            <path d="M9 1L1.92893 8.07107L9 15.1421" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <span class="text">Back</span>
                    </a>
                </div>
                <div class="mid bg-white basis-full lg:pt-[3.75rem] lg:px-[4.375rem] lg:basis-[72%] lg:-mt-[7.5rem] relative">
                    <h1 class="mb-2.5">The Vanport Flood</h1>
                    <p class="written-by block text-gray-200 mb-[2.125rem]">Written by Michael N. McGregor</p>
                    <div class="wrapper block lg:hidden mb-[2.125rem]">
                        <a href="#" class="btn-link alt">
                            <span class="text">View on Wayfinder</span>
                            <svg class="ml-1.5" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" fill="none">
                                <path d="M29.5 14.719C29.5 22.5631 23.0171 28.938 15 28.938C6.98292 28.938 0.5 22.5631 0.5 14.719C0.5 6.87495 6.98292 0.5 15 0.5C23.0171 0.5 29.5 6.87495 29.5 14.719Z" stroke="#232D33"/>
                                <path d="M14.9998 6.8689C13.4073 6.87219 11.8812 7.48634 10.7563 8.57657C9.63152 9.66679 8.99987 11.144 9 12.6841C9 15.4198 11.1871 17.5294 12.8174 19.5653C13.5411 20.4689 14.2685 21.3695 14.9998 22.2673C15.7307 21.3695 16.4581 20.4689 17.1821 19.5653C18.8124 17.5294 21 15.4198 21 12.6841C20.9999 11.1441 20.3681 9.66695 19.2432 8.57684C18.1183 7.48672 16.5922 6.87265 14.9998 6.86936V6.8689ZM14.9998 15.0326C14.5143 15.0326 14.0397 14.8933 13.636 14.6325C13.2324 14.3716 12.9178 14.0009 12.732 13.5671C12.5463 13.1333 12.4977 12.656 12.5924 12.1955C12.6872 11.735 12.921 11.3121 13.2643 10.9801C13.6076 10.6482 14.045 10.4221 14.5212 10.3306C14.9974 10.239 15.4909 10.2861 15.9394 10.4658C16.3879 10.6456 16.7712 10.9499 17.0409 11.3403C17.3105 11.7308 17.4544 12.1897 17.4543 12.6593C17.4543 12.971 17.3908 13.2797 17.2675 13.5677C17.1441 13.8557 16.9633 14.1173 16.7354 14.3378C16.5075 14.5582 16.2369 14.733 15.9391 14.8523C15.6413 14.9716 15.3221 15.033 14.9998 15.033V15.0326Z" stroke="#232D33"/>
                            </svg>
                        </a>
                    </div>
                    <p>On Memorial Day in 1948, the Columbia River, roaring downstream fifteen feet above the flood plain in Portland, undermined a railroad embankment that served as a dike, starting a flood that would leave 18,000 people homeless and significantly alter race relations in Portland.</p>
                    <p>For eight years, the embankment had kept the Columbia River out of the newly developed 648-acre complex named Vanport (Vancouver + Portland), then the largest public housing project in the United States. Meant to be temporary, Vanport was shipbuilding magnate Henry Kaiser's answer to a lack of housing in the early days of World War II, when he was recruiting men and women from across the United States to work in his Portland-area shipyards. At the height of the war in 1944, over 40,000 people lived in Vanport, including up to 10,000 African Americans, over three times as many as had lived in all of Portland two years before.</p>
                    <p>Kaiser had just erected the first of his shipyards on the Willamette River when America entered the war in 1941. As America's involvement in the war escalated, and the demand for ships grew, he added two more shipyards in the Portland area, but he could not find enough local workers to fill them. He decided to recruit across the country, offering high wages and free transportation. Before his recruitment campaign was a year old, nearly 100,000 workers had flocked to the shipyards. The rapidly growing population in the Portland and Vancouver area led to a serious housing shortage, which was especially difficult for incoming black workers and their families, who encountered a dominantly white city reluctant to welcome them.</p>
                    <p>When Kaiser began recruiting workers, he hoped Portland would build temporary public housing to accommodate them, as other cities across the country had done when they became war industry centers. But the real estate industry balked at having public housing compete with private interests, and the newly created Housing Authority of Portland was slow to respond. At the same time, the private housing market was too limited to handle the influx, and real estate practices, written into the real estate industry's Code of Ethics, restricted African Americans from living in most parts of the city.</p>
                </div>
                <div class="right hidden lg:block basis-full lg:basis-[10.75rem] shrink-0">
                    <a href="#" class="btn-link alt mb-2.5">
                        <span class="text">View on Wayfinder</span>
                        <svg class="ml-1.5" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" fill="none">
                            <path d="M29.5 14.719C29.5 22.5631 23.0171 28.938 15 28.938C6.98292 28.938 0.5 22.5631 0.5 14.719C0.5 6.87495 6.98292 0.5 15 0.5C23.0171 0.5 29.5 6.87495 29.5 14.719Z" stroke="#232D33"/>
                            <path d="M14.9998 6.8689C13.4073 6.87219 11.8812 7.48634 10.7563 8.57657C9.63152 9.66679 8.99987 11.144 9 12.6841C9 15.4198 11.1871 17.5294 12.8174 19.5653C13.5411 20.4689 14.2685 21.3695 14.9998 22.2673C15.7307 21.3695 16.4581 20.4689 17.1821 19.5653C18.8124 17.5294 21 15.4198 21 12.6841C20.9999 11.1441 20.3681 9.66695 19.2432 8.57684C18.1183 7.48672 16.5922 6.87265 14.9998 6.86936V6.8689ZM14.9998 15.0326C14.5143 15.0326 14.0397 14.8933 13.636 14.6325C13.2324 14.3716 12.9178 14.0009 12.732 13.5671C12.5463 13.1333 12.4977 12.656 12.5924 12.1955C12.6872 11.735 12.921 11.3121 13.2643 10.9801C13.6076 10.6482 14.045 10.4221 14.5212 10.3306C14.9974 10.239 15.4909 10.2861 15.9394 10.4658C16.3879 10.6456 16.7712 10.9499 17.0409 11.3403C17.3105 11.7308 17.4544 12.1897 17.4543 12.6593C17.4543 12.971 17.3908 13.2797 17.2675 13.5677C17.1441 13.8557 16.9633 14.1173 16.7354 14.3378C16.5075 14.5582 16.2369 14.733 15.9391 14.8523C15.6413 14.9716 15.3221 15.033 14.9998 15.033V15.0326Z" stroke="#232D33"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>

        <div class="wrapper carousel-curator owl-theme owl-carousel w-screen">
            <div class="card v13">
                <a href="#" class="image auto mb-4">
                    <img src="./img/placeholder/curator-article-slide-1.jpg" alt="Alt Text Here" />
                    <span class="square-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="29" height="30" viewBox="0 0 29 30" fill="none">
                            <circle cx="11.5485" cy="12.5251" r="7.16327" transform="rotate(-45 11.5485 12.5251)" stroke="white" stroke-width="2"/>
                            <path d="M17.0035 18.0377L22.7506 23.6862" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M11.5 10V15" stroke="white" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                            <path d="M9 12.5L14 12.5" stroke="white" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                        </svg>
                    </span>
                </a>
                <h2 class="title mb-0">Black troops from Fort Lewis work on flood control in Vanport, 1948 <span>Courtesy Oregon Hist. Soc. Research Lib., 021630.</span></h2>
            </div>
            <div class="card v13">
                <a href="#" class="image auto mb-4">
                    <img src="./img/placeholder/curator-article-slide-2.jpg" alt="Alt Text Here" />
                    <span class="square-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="29" height="30" viewBox="0 0 29 30" fill="none">
                            <circle cx="11.5485" cy="12.5251" r="7.16327" transform="rotate(-45 11.5485 12.5251)" stroke="white" stroke-width="2"/>
                            <path d="M17.0035 18.0377L22.7506 23.6862" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M11.5 10V15" stroke="white" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                            <path d="M9 12.5L14 12.5" stroke="white" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                        </svg>
                    </span>
                </a>
                <h2 class="title mb-0">Map of Vanport. <span>Oregon Historical Society, Neg. OrHi 94480.</span></h2>
            </div>
            <div class="card v13">
                <a href="#" class="image auto mb-4">
                    <img src="./img/placeholder/curator-article-slide-3.jpg" alt="Alt Text Here" />
                    <span class="square-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="29" height="30" viewBox="0 0 29 30" fill="none">
                            <circle cx="11.5485" cy="12.5251" r="7.16327" transform="rotate(-45 11.5485 12.5251)" stroke="white" stroke-width="2"/>
                            <path d="M17.0035 18.0377L22.7506 23.6862" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M11.5 10V15" stroke="white" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                            <path d="M9 12.5L14 12.5" stroke="white" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                        </svg>
                    </span>
                </a>
                <h2 class="title mb-0">People viewing flood damage during the Vanport flood. <span>Oregon Historical Society, OrgLot131_004.</span></h2>
            </div>
        </div>

        <div class="container pt-[1.875rem] md:pt-[3.125rem]">
            <div class="wrapper flex">
                <div class="left hidden lg:block basis-full lg:basis-[10.75rem] shrink-0"></div>
                <div class="mid basis-full lg:px-[4.375rem] lg:basis-[72%]">
                    <p>Kaiser's son Edgar responded by purchasing a floodplain outside the city's limits (and its authority) and, with federal funds and cheap material, built what became the second-largest city in Oregon. During its short life, Vanport would be called everything from a "Miracle City" to a "Masterpiece of Urban Planning" to the "Northwest's unique sociological experiment" to the "Negro project."</p>
                    <p>Life in Vanport was busy. With over 40,000 people pressed together in thin-walled buildings, some working days and others nights, noise was constant. Because both mothers and fathers worked, adequate childcare was a necessity. The town's location along the Columbia floodplain, combined with the wet Northwest weather, often left residents slogging through mud. For many, though, it was a dynamic and diverse community that soon felt like home.</p>
                    <p>Portland had long had a reputation as what one national black leader called "the most prejudiced [city] in the west," a place where the small African American population had been economically and culturally discriminated against. As a result, only 2,000 blacks lived in the city in the years before the war. In the rest of Oregon, the black population was very small. This was due in part to Oregon's state constitution, which had once prohibited blacks from residing in the state, and to the activities of the Ku Klux Klan, which in the 1920s had up to 35,000 Oregon members at its height.</p>
                    <div class="btn-wrap text-center mt-11 md:mt-[4.875rem]"><a href="#" class="btn outline-gray">Load More</a></div>
                </div>
                <div class="right hidden lg:block basis-full lg:basis-[10.75rem] shrink-0"></div>
            </div>
        </div>

    </section><!-- End of section-->

    <section class="bg-gray-400  pt-[1.875rem] pb-11 md:py-[3.75rem]">
        <div class="container">
            <div class="wrapper flex flex-wrap justify-center xl:ml-[7.5rem] xl:mr-[6.438rem] gap-y-3.5 lg:gap-y-[5.8%] xl:gap-y-0 lg:gap-x-[5.8%]">
                <div class="basis-full lg:basis-[47.1%]">
                    <h2 class="text-20 mb-3 md:mb-[1.875rem]">Related Historical Records</h2>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title !text-soft-black mb-1.5">Fair Housing in Oregon Study</h4>
                            <p class="!text-soft-black">This case study was included as an appendix by the League of Women Voters of Portland in A Study of Awareness of the Oregon Fair Housing Law and a Sampling of...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-5.jpg" alt="Alt Text Here" /></div>
                    </a>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title !text-soft-black mb-1.5">Kaiser & Oregon Shipyards</h4>
                            <p class="!text-soft-black">In 1940, Henry J. Kaiser signed an agreement with the British government to build 31 cargo ships to aid that country in their war effort.  After scouting several sites, Kaiser...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-6.jpg" alt="Alt Text Here" /></div>
                    </a>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title !text-soft-black mb-1.5">Vanport Residences, 1947</h4>
                            <p class="!text-soft-black">Built to address Portland’s World War II housing shortage, Vanport was called “The Miracle City.” Over 72,000 new workers arrived in Portland during ...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-7.jpg" alt="Alt Text Here" /></div>
                    </a>
                </div>
                <div class="basis-full lg:basis-[47.1%]">
                    <h2 class="text-20 mb-3 md:mb-[1.875rem]">Related Oregon Encyclopedia Articles</h2>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title !text-soft-black mb-1.5">Vanport</h4>
                            <p class="!text-soft-black">In its short history, from 1942 to 1948, Vanport was the nation’s largest wartime housing development, a site for social innovation, a lightning rod for racial prejudice, and the scene ...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-8.jpg" alt="Alt Text Here" /></div>
                    </a>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title !text-soft-black mb-1.5">Vanport Extension Center</h4>
                            <p class="!text-soft-black">The Vanport Extension Center grew from a converted shopping mall and recreation center in the World War II city of Vanport into Portland State University, the first publicly ...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-9.jpg" alt="Alt Text Here" /></div>
                    </a>
                </div>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>