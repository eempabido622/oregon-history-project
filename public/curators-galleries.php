<?php include "./header.html"; ?>
<article>
    <section class="page-banner"></section><!-- End of page-banner-->
    <section class="bg-gray-400 pb-[1.875rem] pt-[4.625rem] md:pt-[9.5rem] xl:pt-[18.125rem] md:pb-[3.125rem]">
        <div class="container">
            <div class="wrapper max-w-[44.688rem] mb-6 md:mb-[2.25rem]">
                <h1>Curator Collections</h1>
                <p>A series of historical records collected, interpreted and associated with topics to guide readers to their interests or school assignments.</p>
            </div>
            <ul class="tabs v2 horizontal mb-0">
                <li class="active"><a href="#">View All</a></li>
                <li><a href="#">Places</a></li>
                <li><a href="#">Personalities</a></li>
                <li><a href="#">Weird But True</a></li>
                <li><a href="#">Equity & Activism</a></li>
                <li><a href="#">Indigenous Culture</a></li>
                <li><a href="#">Documents</a></li>
                <li><a href="#">Natural Wonders</a></li>
                <li><a href="#">Oregon Traditions</a></li>
                <li><a href="#">Events</a></li>
                <li><a href="#">War & Peace</a></li>
            </ul>
        </div>
    </section>
    <section class="bg-gray-400 pt-0 pb-[1.875rem] md:pt-0 md:pb-[8.5rem]">
        <div class="divider xl:ml-[3.75rem] w-full border-b border-b-gray-100/20"></div>
        <div class="container pt-[1.875rem] md:pt-[4.375rem]">
            <div class="wrapper flex flex-wrap items-start justify-center md:justify-between md:gap-x-[5.3%]"> 
                <div class="card v10 basis-full md:basis-[70%] lg:basis-[69.7%] md:ml-auto">
                    <a href="#" title="Map Gallery" class="image auto mb-6"><img src="./img/placeholder/gallery-image-1.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title mb-2"><a href="#">Map Gallery</a></h2>
                    <p>Select maps from the collection; there are over 25,000 maps held by the Oregon Historical Society Research Library.</p>
                    <div class="tags"><a href="#">Places</a></div>
                </div>
                <div class="card v10 md:basis-[55%] lg:basis-[49.5%]"> 
                    <a href="#" title="Crime & Punishment" class="image auto mb-6"><img src="./img/placeholder/gallery-image-2.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title mb-2"><a href="#">Crime & Punishment</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Amet leo, montes, commodo elit blandit cras sit dignissim. Aliquet id fermentum vitae arcu, sem consectetur turpis.</p>
                    <div class="tags"><a href="#">Personalities</a></div>
                </div>
                <div class="card v10 md:basis-[35%] lg:basis-[29.4%] mt-0 md:mt-[8.75rem]">
                    <a href="#" title="Oregon’s Literary Traditions" class="image auto mb-6"><img src="./img/placeholder/gallery-image-3.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title mb-2"><a href="#">Oregon’s Literary Traditions</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eu enim sagittis ut. Pretium risus at nisi, lectus.</p>
                    <div class="tags"><a href="#">Personalities</a></div>
                </div>
                <div class="card v10 basis-full md:basis-[70%] lg:basis-[69.7%]">
                    <a href="#" title="Pendleton Roundup" class="image auto mb-6"><img src="./img/placeholder/gallery-image-4.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title mb-2"><a href="#">Pendleton Roundup</a></h2>
                    <p>Throughout its 100-year history, the Native American participation, strong volunteer corps, and community support have made the Pendleton Roundup one of the largest and most beloved rodeos in the country.</p>
                    <div class="tags"><a href="#">Oregon Traditions</a><span class="separator">|</span><a href="#">Events</a></div>
                </div>
                <div class="card v10 md:basis-[35%] lg:basis-[29.4%] mt-0 md:mt-[10.125rem]">
                    <a href="#" title="Women in the Shipyards" class="image auto mb-6"><img src="./img/placeholder/gallery-image-5.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title mb-2"><a href="#">Women in the Shipyards</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Malesuada nisl velit integer ac senectus dui id quis eget. Dolor quis eget cras porttitor ridiculus nulla ut lectus. </p>
                    <div class="tags"><a href="#">WAR & PEACE</a></div>
                </div>
                <div class="card v10 md:basis-[55%] lg:basis-[49.5%]">
                    <a href="#" title="Oregon’s History of Activism" class="image auto mb-6"><img src="./img/placeholder/gallery-image-6.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title mb-2"><a href="#">Oregon’s History of Activism</a></h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Purus in malesuada diam id pulvinar quisque. Aliquam arcu ipsum commodo eget. Et sapien ornare vitae euismod malesuada consequat.</p>
                    <div class="tags"><a href="#">Equity & Activism</a></div>
                </div>
            </div>
            <div class="btn-wrap text-center"><a href="#" class="btn outline-gray">Load More</a></div>
        </div>
    </section><!-- End of section-->

    <section class="bg-soft-black pt-[1.875rem] pb-9 md:pb-[5.688rem] md:pt-[3.75rem]">
        <div class="container">
            <div class="head flex items-center justify-between">
                <h2 class="text-32 text-gold-100 mb-1">Narratives</h2>
                <a href="#" class="btn-link hidden md:flex">
                    <span class="text mr-2.5">View All</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
                        <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#D3B960" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            </div>
            <div class="carousel-narratives grid grid-cols-5 lg:gap-x-4 xl:gap-x-[3.125rem] mt-6 md:mt-[2.875rem]">
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-1.jpg" alt="This Land, Oregon" /></div>
                <h3 class="title mb-0">This Land, Oregon</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-2.jpg" alt="Canneries on the Columbia" /></div>
                <h3 class="title mb-0">Canneries on the Columbia</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-3.jpg" alt="Commerce, Climate, and Community: A History of Portland and its People" /></div>
                <h3 class="title mb-0">Commerce, Climate, and Community: A History of Portland and its People</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-4.jpg" alt="igh Desert History: Southeastern Oregon" /></div>
                <h3 class="title mb-0">High Desert History: Southeastern Oregon</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-5.jpg" alt="The Oregon Coast—Forists and Green Verdent Launs" /></div>
                <h3 class="title mb-0">The Oregon Coast—"Forists and Green Verdent Launs"</h3>
            </a>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>