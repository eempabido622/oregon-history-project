<?php include "./header.html"; ?>
<article>
    <section class="page-banner min-h-0 md:min-h-[35rem]" style="background-image: url('./img/placeholder/records-search-banner.jpg');">
        <div class="container justify-center pt-[5.813rem] md:pt-0 md:justify-end">
            <div class="inner max-w-[44.688rem] pb-[1.875rem] md:pb-10 xl:pb-20">
            <h1 class="heading text-white mb-1 md:mb-4">Historical Records</h1>
            <p class="text-white mb-4 md:mb-[1.875rem]">Historical records furnish the raw data researchers use to understand the past. Interpreting multiple points of context and viewpoints help us better understand the story of Oregon.</p>
            <form class="search-form mb-0">
                <label for="search" class="hidden">Find a record</label>
                <input type="text" name="search" id="search" placeholder="Find a record" value="Oregon Territory" />
                <input type="submit" value="Search" />
            </form>
            </div>
        </div>
    </section><!-- End of page-banner-->

    <section class="bg-white pt-4 pb-[1.875rem] md:py-20">
        <div class="container">
            <div class="wrapper flex flex-wrap gap-x-[6.6%]">
                <div class="basis-full lg:basis-[26.7%] mb-8 lg:mb-0">
                    <form class="form-default">
                        <h2 class="form-heading mb-6">Refine your search</h2>
                        <div class="form-field mb-5">
                            <label for="eras" class="mb-3">Era</label>
                            <select name="eras" id="eras"><option>Select Eras…</option></select>
                        </div>
                        <div class="form-field mb-5">
                            <label for="region" class="mb-3">Region</label>
                            <select name="region" id="region"><option>Select Region…</option></select>
                        </div>
                        <div class="form-field mb-5">
                            <label for="county" class="mb-3">County</label>
                            <select name="county" id="county"><option>Select County…</option></select>
                        </div>
                        <div class="form-field mb-[2.313rem]">
                            <label for="theme" class="mb-3">Theme</label>
                            <select name="theme" id="theme"><option>Select Theme…</option></select>
                        </div>
                        <div class="divider border-b border-b-gray-100 mb-[2.313rem]"></div>
                        <h2 class="form-heading mb-[0.938rem]">Enrich your results</h2>
                        <div class="form-field flex items-start md:max-w-[16rem] mb-[0.688rem]">
                            <input type="checkbox" id="narratives" name="narratives" >
                            <label for="narratives" class="input-label ml-3.5 mb-0">Include narratives written by historians based on OHP records</label>
                        </div>
                        <div class="form-field flex items-start md:max-w-[16rem] mb-0">
                            <input type="checkbox" id="story" name="story" >
                            <label for="story" class="input-label ml-3.5 mb-0">Include stories about Oregon from the Oregon Encyclopedia</label>
                        </div>
                    </form>
                </div>
                <div class="relative basis-full lg:basis-[66.7%]">
                    <div class="no-results">
                        <img src="./img/placeholder/no-results.jpg" alt="Alt Text Here" class="block mb-[1.625rem]" width="353" height="auto" />
                        <p>We couldn't find any records matching that search.</p><p>Please check your search criteria and try again.</p>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-400  pt-[1.875rem] pb-11 md:py-[3.75rem]">
        <div class="container">
            <div class="head flex items-center justify-between">
                <h2 class="text-32 mb-1">Curator Articles</h2>
                <a href="#" class="btn-link hidden md:flex">
                    <span class="text mr-2.5">View All</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
                        <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            </div>
            <div class="wrapper lg:max-w-[65.9%]">
                <p>Curator articles use primary documents from the Oregon Historical Society archives to help readers imagine the events, people, and issues that shaped Oregon history.</p>
            </div>
            <div class="carousel-articles grid grid-cols-3 md:gap-x-4 xl:gap-x-[3.75rem] mt-6 md:mt-[2.875rem]">
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-article-1.jpg" alt="Alt Text Here" /></div>
                <h3 class="title h4 mb-0">The Vanport Flood</h3>
            </a>
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-article-2.jpg" alt="Alt Text Here" /></div>
                <h3 class="title h4 mb-0">Abigail Scott Duniway's Quilt </h3>
            </a>
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-article-3.jpg" alt="Alt Text Here" /></div>
                <h3 class="title h4 mb-0">A Look Back At Portland Jazz: When the Joint Was Jumpin'</h3>
            </a>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>