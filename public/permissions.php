<?php include "./header.html"; ?>
<article>
    <section class="page-banner"></section><!-- End of page-banner-->
    <section class="bg-gray-400 pb-[1.875rem] pt-[4.625rem] md:pt-[6.25rem] xl:pt-[18.75rem] md:pb-[4.375rem]">
        <div class="container">
            <div class="wrapper max-w-[44.688rem]">
                <h1 class="h3">Permissions</h1>
                <p>The Oregon Historical Society (OHS) owns the content on the Oregon History Project. All materials, including essays and images, may not be reproduced in print or electronically without written permission from the Oregon Historical Society.</p>
                <p>Teachers and educational organizations may use the content on the Oregon History Project for educational purposes only. They do not need to obtain permission to use site materials for classroom exercises or teacher workshops. Website administrators may create a direct link to the Oregon History Project from their websites without obtaining permission from OHS. Teachers and educational organizations may not reproduce OHP content in publications of any kind — paper or electronic — or on websites without written permission from the Oregon Historical Society.</p>
                <p>The reproduction of content of any length without express permission from OHS is a violation of copyright even if the content is attributed to the Oregon Historical Society.</p>
                <p>Find more information on the use and reproduction of OHS images <a href="#">here</a>. </p>
                <p>All other requests should be sent to the OHS Research Library: <a href="#">libreference@OHS.org</a>, Attn: Amy Platt, Oregon History Project Manager. </p>
            </div>
        </div>
    </section><!-- End of section-->

</article>
<?php include "./footer.html"; ?>