<?php include "./header.html"; ?>
<article>
    <section class="page-banner"></section><!-- End of page-banner-->
    <section class="bg-gray-400 pb-[1.875rem] pt-[4.625rem] md:pt-[6.25rem] xl:pt-10 md:pb-[3.125rem]">
        <div class="container">
            <div class="image lg:max-w-[41rem] lg:ml-auto mb-6 md:mb-[1.875rem] lg:mb-[2.375rem]">
                <img src="./img/placeholder/curators-gallery-detail-image.jpg" alt="Alt Text Here" />
                <p class="image-caption text-sm font-proxima text-gray-200 mt-3 mb-0"><span class="font-semibold">Image Title.</span> Oregon Historical Society, OrgLot131_004.</p>
            </div>
            <div class="wrapper max-w-[44.688rem]">
                <h1>Women in the Shipyards</h1>
                <p>Oregon women joined millions of women across the country who found meaningful employment in war-related industries. Women who had been restricted to particular jobs—or to no jobs at all because of their gender and race—worked with men in factories and farms The increased democratization of the labor force in the United States during World War II was a consequence of the desperate need for workers to begin working immediately. </p>
            </div>
        </div>
    </section>
    <section class="bg-gray-400 pt-0 pb-[1.875rem] md:pt-0 md:pb-[8.5rem]">
        <div class="divider xl:ml-[3.75rem] w-full border-b border-b-gray-100/20"></div>
        <div class="container pt-[1.875rem] md:pt-[4.375rem]">
            <div class="card v11">
                <a href="#" class="image auto"><img src="./img/placeholder/curators-gallery-detail-image-1.jpg" alt="Alt Text Here" /></a>
                <div class="details">
                    <h2 class="title text-32 mb-4"><a href="#">Iona Murphy at Oregon Shipbuilding Corp., Portland</a></h2>
                    <p>This ca. 1943 photograph, taken by Ray Atkeson, shows Iona Murphy welding in an assembly building at the Oregon Shipbuilding Corporation in Portland. During World War II, up to 30,000 women worked in shipyards in Portland and Vancouver, Washington, building tankers, aircraft carriers, and merchant marine transportation ships for the war effort. The Oregon Shipbuilding Corporation was one of three shipyards owned locally by the Kaiser Corporation. Atkeson was a photographer who documented Oregon’s people and landscapes from 1928 until his death in 1990.</p>
                    <div class="btn-wrap">
                        <a href="#" class="btn-link">
                            <span class="text">Read More</span>
                            <svg class="ml-2.5" xmlns="http://www.w3.org/2000/svg" width="10" height="17" viewBox="0 0 10 17" fill="none">
                                <path d="M1.07031 1.70801L8.14138 8.77908L1.07031 15.8501" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card v11">
                <a href="#" class="image auto"><img src="./img/placeholder/curators-gallery-detail-image-2.jpg" alt="Alt Text Here" /></a>
                <div class="details">
                    <h2 class="title text-32 mb-4"><a href="#">Iona Murphy at Oregon Shipbuilding Corp., Portland</a></h2>
                    <p>This ca. 1943 photograph, taken by Ray Atkeson, shows Iona Murphy welding in an assembly building at the Oregon Shipbuilding Corporation in Portland. During World War II, up to 30,000 women worked in shipyards in Portland and Vancouver, Washington, building tankers, aircraft carriers, and merchant marine transportation ships for the war effort. The Oregon Shipbuilding Corporation was one of three shipyards owned locally by the Kaiser Corporation. Atkeson was a photographer who documented Oregon’s people and landscapes from 1928 until his death in 1990.</p>
                    <div class="btn-wrap">
                        <a href="#" class="btn-link">
                            <span class="text">Read More</span>
                            <svg class="ml-2.5" xmlns="http://www.w3.org/2000/svg" width="10" height="17" viewBox="0 0 10 17" fill="none">
                                <path d="M1.07031 1.70801L8.14138 8.77908L1.07031 15.8501" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card v11">
                <a href="#" class="image auto"><img src="./img/placeholder/curators-gallery-detail-image-3.jpg" alt="Alt Text Here" /></a>
                <div class="details">
                    <h2 class="title text-32 mb-4"><a href="#">The Bo's'n's Whistle</a></h2>
                    <p>The Bo’s’n’s Whistle was an in-house publication distributed to employees of the Oregon Shipbuilding Corporation (OSC), owned by Henry Kaiser. The name comes from the bosun’s whistle, an instrument used by naval boatswains to alert crewmembers to ship commands. Kaiser built and operated three shipyards along the Willamette and Columbia Rivers between 1941 and 1945—two in the Portland area and one in Vancouver, Washington— which employed tens of thousands of men and women.</p>
                    <div class="btn-wrap">
                        <a href="#" class="btn-link">
                            <span class="text">Read More</span>
                            <svg class="ml-2.5" xmlns="http://www.w3.org/2000/svg" width="10" height="17" viewBox="0 0 10 17" fill="none">
                                <path d="M1.07031 1.70801L8.14138 8.77908L1.07031 15.8501" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card v11">
                <a href="#" class="image auto"><img src="./img/placeholder/curators-gallery-detail-image-4.jpg" alt="Alt Text Here" /></a>
                <div class="details">
                    <h2 class="title text-32 mb-4"><a href="#">Handbook for New Women Shipyard Workers</a></h2>
                    <p>In 1943 Portland Public Schools produced a handbook designed to orient new women workes to life in the shipyards. One section dealt with the problems of childcare. 
                    <p>During World War II, women were actively recruited for employment in the nation’s defense industries. In Oregon that meant laboring in the shipyards. Such employment proved attractive, offering working women a considerable advance in wages and the opportunity to perform skilled labor previously the domain of men. In addition, many valued the work as a substantial contribution to the war effort.</p>
                    <div class="btn-wrap">
                        <a href="#" class="btn-link">
                            <span class="text">Read More</span>
                            <svg class="ml-2.5" xmlns="http://www.w3.org/2000/svg" width="10" height="17" viewBox="0 0 10 17" fill="none">
                                <path d="M1.07031 1.70801L8.14138 8.77908L1.07031 15.8501" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-600 py-[1.875rem] pb-11 md:py-[3.75rem]">
        <div class="container">
            <div class="head flex items-center justify-between">
                <h2 class="text-32 mb-1">Curator Collections</h2>
                <a href="#" class="btn-link hidden md:flex">
                    <span class="text mr-2.5">View All</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
                        <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            </div>
            <div class="wrapper lg:max-w-[65.9%]">
            <p>A series of  hand-picked dolor sit amet, consectetur adipiscing elit. Ut lorem vulputate tortor posuere netus magnis. Ullamcorper tincidunt mattis morbi porta elit nulla turpis.</p>
            </div>
            <div class="carousel-articles grid grid-cols-3 md:gap-x-4 xl:gap-x-[3.75rem] mt-6 md:mt-[2.875rem]">
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-collection-1.jpg" alt="Alt Text Here" /></div>
                <h3 class="title mb-0">Oregon’s History of Activism</h3>
            </a>
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-collection-2.jpg" alt="Alt Text Here" /></div>
                <h3 class="title mb-0">Commune Communities</h3>
            </a>
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-collection-3.jpg" alt="Alt Text Here" /></div>
                <h3 class="title mb-0">Oregon’s Literary Traditions</h3>
            </a>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-soft-black pt-[1.875rem] pb-[1.875rem] md:pb-[5.688rem] md:pt-[3.75rem]">
        <div class="container">
            <div class="head flex items-center justify-between">
                <h2 class="text-32 text-gold-100 mb-1">Narratives</h2>
                <a href="#" class="btn-link hidden md:flex">
                    <span class="text mr-2.5">View All</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
                        <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#D3B960" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            </div>
            <div class="carousel-narratives grid grid-cols-5 lg:gap-x-4 xl:gap-x-[3.125rem] mt-6 md:mt-[2.875rem]">
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-1.jpg" alt="This Land, Oregon" /></div>
                <h3 class="title mb-0">This Land, Oregon</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-2.jpg" alt="Canneries on the Columbia" /></div>
                <h3 class="title mb-0">Canneries on the Columbia</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-3.jpg" alt="Commerce, Climate, and Community: A History of Portland and its People" /></div>
                <h3 class="title mb-0">Commerce, Climate, and Community: A History of Portland and its People</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-4.jpg" alt="igh Desert History: Southeastern Oregon" /></div>
                <h3 class="title mb-0">High Desert History: Southeastern Oregon</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-5.jpg" alt="The Oregon Coast—Forists and Green Verdent Launs" /></div>
                <h3 class="title mb-0">The Oregon Coast—"Forists and Green Verdent Launs"</h3>
            </a>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>