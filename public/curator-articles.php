<?php include "./header.html"; ?>
<article>
    <section class="page-banner"></section><!-- End of page-banner-->
    <section class="bg-gray-400 pb-[1.875rem] pt-[4.625rem] md:pt-[9.5rem] xl:pt-[18.125rem] md:pb-[3.125rem]">
        <div class="container">
            <div class="wrapper max-w-[44.688rem] mb-6 md:mb-[2.25rem]">
                <h1>Curator Articles</h1>
                <p>Articles use journals, autobiographies, letters, newspapers, photographs, and other primary documents from the Oregon Historical Society archives. Nonfiction storytelling can open up dialogue and help readers imagine the events, people, and issues that shape Oregon history.</p>
            </div>
            <ul class="tabs v2 horizontal mb-0">
                <li class="active"><a href="#">View All</a></li>
                <li><a href="#">Force Majeure</a></li>
                <li><a href="#">Movements</a></li>
                <li><a href="#">The Arts</a></li>
                <li><a href="#">Equity & Activism</a></li>
                <li><a href="#">Indigenous Stories</a></li>
                <li><a href="#">War & Peace</a></li>
            </ul>
        </div>
    </section>
    <section class="bg-gray-400 pt-0 pb-11 md:pt-0 md:pb-[4.813rem]">
        <div class="divider xl:ml-[3.75rem] w-full border-b border-b-gray-100/20"></div>
        <div class="container pt-[1.875rem] md:pt-[4.375rem]">
            <div class="wrapper grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-[1.875rem] md:gap-10 xl:gap-x-[4.25rem] xl:gap-y-[4.875rem]"> 
                <div class="card v12 big lg:col-span-2 lg:row-span-2">
                    <a href="#" title="The Vanport Flood" class="image mb-5 lg:mb-[1.875rem]"><img src="./img/placeholder/curator-articles-image-1.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title !text-[1.375rem] !leading-[1.25] lg:!text-[2rem] lg:!leading-[2.5rem] mb-1.5 lg:mb-2"><a href="#">The Vanport Flood</a></h2>
                    <p class="mb-4">by Michael McGregor</p>
                    <div class="tags"><a href="#">Force Majeure</a>, <a href="#">Equity & Activism</a></div>
                </div>
                <div class="card v12"> 
                    <a href="#" title="Abigail Scott Duniway's Quilt " class="image mb-5"><img src="./img/placeholder/curator-articles-image-2.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Abigail Scott Duniway's Quilt</a></h2>
                    <p class="mb-4">by Michael McGregor</p>
                    <div class="tags"><a href="#">Equity & Activism</a></div>
                </div>
                <div class="card v12"> 
                    <a href="#" title="The Legacy of Mr. Jones: Spreading Old World Contagions " class="image  mb-5"><img src="./img/placeholder/curator-articles-image-3.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">The Legacy of Mr. Jones: Spreading Old World Contagions</a></h2>
                    <p class="mb-4">by Michael McGregor</p>
                    <div class="tags"><a href="#">Indigenous Stories</a>, <a href="#">War & Peace</a></div>
                </div>
                <div class="card v12"> 
                    <a href="#" title="The Death of Peter French: Clashes of Cattlemen and Settlers" class="image mb-5"><img src="./img/placeholder/curator-articles-image-4.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">The Death of Peter French: Clashes of Cattlemen and Settlers</a></h2>
                    <p class="mb-4">by Michael McGregor</p>
                    <div class="tags"><a href="#">Category Label</a></div>
                </div>
                <div class="card v12"> 
                    <a href="#" title="The Legacy of Mr. Jones: Spreading Old World Contagions " class="image  mb-5"><img src="./img/placeholder/curator-articles-image-5.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">A Look Back At Portland Jazz: When the Joint Was Jumpin'</a></h2>
                    <p class="mb-4">by Michael McGregor</p>
                    <div class="tags"><a href="#">Category Label</a></div>
                </div>
                <div class="card v12"> 
                    <a href="#" title="Future Article" class="image mb-5"><img src="./img/placeholder/curator-articles-image-6.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Future Article</a></h2>
                    <p class="mb-4">by Michael McGregor</p>
                    <div class="tags"><a href="#">Category Label</a></div>
                </div>
                <div class="card v12"> 
                    <a href="#" title="Future Article" class="image  mb-5"><img src="./img/placeholder/curator-articles-image-7.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Future Article</a></h2>
                    <p class="mb-4">by Michael McGregor</p>
                    <div class="tags"><a href="#">Category Label</a></div>
                </div>
                <div class="card v12"> 
                    <a href="#" title="Future Article" class="image mb-5"><img src="./img/placeholder/curator-articles-image-8.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Future Article</a></h2>
                    <p class="mb-4">by Michael McGregor</p>
                    <div class="tags"><a href="#">Category Label</a></div>
                </div>
                <div class="card v12"> 
                    <a href="#" title="Interpretive Essay Bibliography" class="image  mb-5"><img src="./img/placeholder/curator-articles-image-9.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Interpretive Essay Bibliography</a></h2>
                    <p class="mb-4">by Michael McGregor</p>
                    <div class="tags"><a href="#">Category Label</a></div>
                </div>
            </div>
            <div class="btn-wrap text-center mt-11 md:mt-[4.875rem]"><a href="#" class="btn outline-gray">Load More</a></div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-600  pt-[1.875rem] pb-11 md:py-[3.75rem]">
        <div class="container">
            <div class="head flex items-center justify-between">
                <h2 class="text-32 mb-1">Curator Collections</h2>
                <a href="#" class="btn-link hidden md:flex">
                    <span class="text mr-2.5">View All</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
                        <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            </div>
            <div class="wrapper lg:max-w-[65.9%]">
            <p>A series of  hand-picked dolor sit amet, consectetur adipiscing elit. Ut lorem vulputate tortor posuere netus magnis. Ullamcorper tincidunt mattis morbi porta elit nulla turpis.</p>
            </div>
            <div class="carousel-articles grid grid-cols-3 md:gap-x-4 xl:gap-x-[3.75rem] mt-6 md:mt-[2.875rem]">
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-collection-4.jpg" alt="Alt Text Here" /></div>
                <h3 class="title mb-0">Oregon’s History of Activism</h3>
            </a>
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-collection-5.jpg" alt="Alt Text Here" /></div>
                <h3 class="title mb-0">Commune Communities</h3>
            </a>
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-collection-6.jpg" alt="Alt Text Here" /></div>
                <h3 class="title mb-0">Oregon’s Literary Traditions</h3>
            </a>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>