<?php include "./header.html"; ?>
<article>
    <section class="page-banner"></section><!-- End of page-banner-->
    <section class="bg-gray-400 pt-[4.625rem] pb-[1.875rem] md:pt-[10.375rem] md:pb-20">
        <div class="container">
            <div class="wrapper grid grid-cols-1 gap-y-[1.875rem] md:gap-y-10 md:grid-cols-2 lg:grid-cols-3 xl:gap-y-[3.875rem] md:gap-x-10 xl:gap-x-[4.563rem]">
                <div class="page-intro">
                    <h1>Narratives</h1>
                    <p>The Oregon History Project provides historical Narratives written by Pacific Northwest historians providing ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="This Land, Oregon"><img src="./img/placeholder/narratives-b-1.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">This Land, Oregon</a></h2>
                    <p>This Land, Oregon, a narrative history of Oregon, is written by one of the Pacific Northwest’s most respected scholars., William G. Robbins.</p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="Lewis and Clark: From Expedition to Exposition, 1803–1905"><img src="./img/placeholder/narratives-b-2.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Lewis and Clark: From Expedition to Exposition, 1803–1905</a></h2>
                    <p>For people living in the Pacific Northwest, few historical events have more imaginative power than the Lewis and Clark Expedition. </p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="Canneries on the Columbia"><img src="./img/placeholder/narratives-b-3.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Canneries on the Columbia</a></h2>
                    <p>“Canneries on the Columbia” is a bit different from the other narratives you’ll find in the Oregon History Project.</p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="Commerce, Climate, and Community: A History of Portland and its People"><img src="./img/placeholder/narratives-b-4.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Commerce, Climate, and Community: A History of Portland and its People</a></h2>
                    <p>Historian William Toll examines the tensions between social classes and ethnic groups in the city of Portland and the emergence of residential patterns and...</p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="High Desert History: Southeastern Oregonz"><img src="./img/placeholder/narratives-b-5.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">High Desert History: Southeastern Oregonz</a></h2>
                    <p>Compared to the rest of the state, southeastern Oregon's high-desert country is a harsh but compelling landscape.</p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="The Oregon Coast—'Forists and Green Verdent Launs'"><img src="./img/placeholder/narratives-b-6.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">The Oregon Coast—"Forists and Green Verdent Launs"</a></h2>
                    <p>The history of the Oregon Coast is a story of the relationship between people and a rugged landscape.</p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="The World Rushed In: Northeastern Oregon"><img src="./img/placeholder/narratives-b-7.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">The World Rushed In: Northeastern Oregon</a></h2>
                    <p>Northeastern Oregon — Grant, Baker, Union, Wallowa, Umatilla, Morrow, Gilliam, Sherman, and Wheeler counties, together with the Ontario/Vale/Nyssa area of...</p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="Wooden Beams and Railroad Ties: The History of Oregon's Built Environment"><img src="./img/placeholder/narratives-b-8.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Wooden Beams and Railroad Ties: The History of Oregon's Built Environment</a></h2>
                    <p>The buildings and structures built by Oregon's inhabitants are evidence of how they lived, their industries and occupations, their creative impulses, and the natural...</p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="Nature and History in the Klamath Basin"><img src="./img/placeholder/narratives-b-9.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Nature and History in the Klamath Basin</a></h2>
                    <p>A land of mountains, forests, wetlands, lakes, and rivers, the Klamath Basin spans the Oregon-California state line and is larger in area than nine of the fifty states...</p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="Oregon Folklife: Our Living Traditions"><img src="./img/placeholder/narratives-b-10.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Oregon Folklife: Our Living Traditions</a></h2>
                    <p>Oregon Folklife: Our Living Traditions explores community-based arts and culture in the context of local history. </p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="Central Oregon: Adaptation and Compromise in an Arid Landscape"><img src="./img/placeholder/narratives-b-11.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">Central Oregon: Adaptation and Compromise in an Arid Landscape</a></h2>
                    <p>Central Oregon is known primarily as an outdoor recreation center but beneath its regional playground surface it possesses a vital history. </p>
                </div>
                <div class="card v7">
                    <a href="#" class="image mb-6" title="As Long as the World Goes On: The Land and People of Southwest Oregon"><img src="./img/placeholder/narratives-b-12.jpg" alt="Alt Text Here" /></a>
                    <h2 class="title h4 mb-1.5"><a href="#">As Long as the World Goes On: The Land and People of Southwest Oregon</a></h2>
                    <p>As Long as the World Goes On explores the geography of southwestern Oregon, the Native cultures of the region, the history of Euro-American contact and ...</p>
                </div>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>