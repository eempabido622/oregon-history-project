<?php include "./header.html"; ?>
<article>
    <section class="page-banner relative block lg:flex lg:flex-row min-h-0 lg:min-h-[26.25rem] bg-gray-100">
        <div class="container relative z-[2] justify-start pt-[4.875rem] items-start pr-[2.375rem] md:pt-[9.5rem] md:pr-[15%] xl:pr-7 lg:pt-0 lg:items-end lg:justify-end">
            <div class="inner max-w-none xl:max-w-[69.5%] pb-[1.875rem] pr-6 md:pr-[7.438rem] md:pb-10 lg:pb-[2.938rem]">
                <span class="subhead text-gray-400 text-sm mb-2 md:mb-4">NARRATIVES</span>
                <h1 class="heading text-white h2 mb-2 md:mb-4">Lewis and Clark: From Expedition to Exposition, 1803–1905</h1>
                <span class="by text-gray-400 text-sm mb-0">by William L. Lang</span>
            </div>
        </div>
        <div class="background absolute flex w-full h-full top-0 left-0 z-[1]">
            <div class="text-col basis-full md:basis-[85%] bg-gray-100"></div>
            <div class="image-col basis-[2.375rem] shrink-0 md:shrink md:basis-[15%]"><img src="./img/placeholder/narrative-overview-banner.jpg" alt="Alt Text Here" /></div>
        </div>
    </section><!-- End of page-banner-->

    <section class="bg-white pt-4 pb-[1.875rem] md:pt-[2.813rem] md:pb-[8.5rem]">
        <div class="container">
            <div class="wrapper flex flex-wrap items-start xl:ml-[2.125rem] xl:mr-[7.438rem] gap-x-[5.3%]">
                <div class="basis-full mb-[1.875rem] lg:basis-[26.4%] lg:mb-0 lg:pr-[2.375rem] lg:border-r lg:border-r-gray-300">
                    <span class="block font-proxima text-gray-200 font-semibold uppercase text-xs tracking-[2px] mb-4">Sections 2/4</span>
                    <h2 class="h4 mb-5">Exploring a Foreign Place: The Lewis and Clark Expedition in Oregon Country</h2>
                    <ul class="tabs vertical v1 list-none pl-0 mb-0 hidden lg:block">
                        <li class="active"><a href="#">Understanding the Expedition</a></li>
                        <li><a href="#">Jefferson's Idea</a></li>
                        <li><a href="#">Jefferson’s Instructions</a></li>
                        <li><a href="#">Louisiana Purchase</a></li>
                        <li><a href="#">Into the New Territory</a></li>
                        <li><a href="#">Across the Plains to Oregon Country</a></li>
                        <li><a href="#">Encounter with the Nez Perce</a></li>
                        <li><a href="#">Down the Columbia</a></li>
                        <li><a href="#">An Inhabited Land</a></li>
                        <li><a href="#">Ocean in View</a></li>
                        <li><a href="#">Stealing a Canoe</a></li>
                        <li><a href="#">Up the Columbia and Back with the Nez Perce</a></li>
                        <li><a href="#">Homeward Bound and a Fatal Encounter</a></li>
                        <li><a href="#">The Return Home</a></li>
                        <li><a href="#">After the Expedition</a></li>
                        <li><a href="#">Clark, Pomp, York, and Sacagawea</a></li>
                        <li><a href="#">Columbia Legacies</a></li>
                    </ul>
                    <form class="form-default block lg:hidden">
                        <div class="form-field">
                            <label for="region" class="!hidden">Section</label>
                            <select name="region" id="region">
                                <option>Understanding the Expedition</option>
                                <option>Jefferson's Idea</option>
                                <option>Jefferson’s Instructions</option>
                                <option>Louisiana Purchase</option>
                                <option>Into the New Territory</option>
                                <option>Across the Plains to Oregon Country</option>
                                <option>Encounter with the Nez Perce</option>
                                <option>Down the Columbia</option>
                                <option>An Inhabited Land</option>
                                <option>Ocean in View</option>
                                <option>Stealing a Canoe</option>
                                <option>Up the Columbia and Back with the Nez Perce</option>
                                <option>Homeward Bound and a Fatal Encounter</option>
                                <option>The Return Home</option>
                                <option>After the Expedition</option>
                                <option>Clark, Pomp, York, and Sacagawea</option>
                                <option>Columbia Legacies</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="relative basis-full lg:basis-[68.3%]">
                    <h2 class="mb-3.5 h3 hidden lg:block">Understanding the Expedition</h2>
                    <p>The Lewis and Clark Expedition is a complex event in American history. The story of the great exploration of the Far West from 1803 to 1806 has been told many times by scholars, essayists, biographers, novelists, and playwrights. Each teller has interpreted events in their own way, seeing the events from different vantage points and emphasizing different themes. This overview is no different. It is written with special emphasis on the time the Corps spent in the Columbia River Basin, in what would become the modern states of Idaho, Washington, and Oregon.</p>
                    <p>When the Corps of Discovery crossed the Continental Divide on their way west in 1805, they entered a vast region that lay beyond the territories claimed by the United States. They were the first Euro Americans to travel by land into the region and everything they saw and recorded in their journals came as new information to the government, to scientists, and to the public. What they did in the Columbia River Basin, who they met, and how they reacted to what they saw had potential and real impact on subsequent events in Oregon. In a genuine way, the Lewis and Clark Expedition is the beginning of the non-Indian history of Oregon.</p>
                    <p>Looking back two hundred years on the historic exploration and reconstructing what happened can be difficult and daunting. Fortunately the explorers left minutely detailed journals of their experiences, but there is so much material on what they saw and did that it is overwhelming. The Journals read like extended field reports, with scientific notations clustered alongside a continuous travelogue that is filled with high adventure. We are struck by the relative lack of personal reflection and opinion, but there is little question that the narrative closely adheres to Jefferson’s original aim in sending an expedition to the West—the discovery of what was out there.</p>
                    <p>Three major themes run through this overview. First, the Expedition began with Thomas Jefferson’s interest in scientific discovery and the region west of the seaboard continental settlements.The Expedition joined his scientific curiosity with geopolitical goals, particularly the expansion of American interest in the lands west of the Mississippi River.Second, national interest in the western regions was about economic development, natural resource wealth, and commerce. Finding a feasible commercial route across the country, preferably by water, became an expeditionary goal. Third, Jefferson’s strong interest in the West’s indigenous population led him to instruct the captains to take note of the Indian nations and document as much about them and their lives as possible. In addition, the president charged the explorers with establishing trading relationships with all willing tribes.</p>
                    <p>What the explorers did in the Columbia River Basin in 1805-1806 depended in large measure on their adherence to the Expedition goals and to their abilities, which were considerable. What happened also depended on what they encountered, especially on the attitudes and actions of Native people. The explorers encountered an unknown and surprising region in the Pacific Northwest, and how they dealt with the contingencies they met bore directly on their fortunes as explorers in the Oregon Country.</p>
                    <span class="post-updated support block !font-proxima mb-6 md:mb-[3.125rem]">&copy; William L. Lang, 2004. Updated and revised by OHP staff, 2014.</span>
                    <div class="divider border-b border-b-gray-300 mb-[1.875rem]"></div>
                    <div class="wrapper flex flex-wrap items-start md:justify-between mb-[3.125rem] md:mb-[4.375rem]">
                        <a href="#" class="page-nav prev">
                            <svg class="mr-0 md:mr-5" xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50" fill="none">
                                <rect x="-1" y="1" width="48" height="48" transform="matrix(-1 0 0 1 48 0)" stroke="#BF3F27" stroke-width="2"/>
                                <path d="M27 17.929L19.9289 25L27 32.0711" stroke="#BF3F27" stroke-width="2" stroke-linecap="square"/>
                            </svg>
                            <span class="text">
                                <span class="label">Prev</span>
                                <span class="title">Introduction</span>
                            </span>
                        </a>
                        <a href="#" class="page-nav next ml-3 md:ml-0">
                            <span class="text">
                                <span class="label md:text-right">Next</span>
                                <span class="title md:text-right">Understanding Jefferson’s Idea</span>
                            </span>
                            <svg class="ml-0 mr-4 md:mr-0 md:ml-5" width="50" height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect x="1" y="1" width="48" height="48" stroke="#BF3F27" stroke-width="2"/>
                                <rect x="1" y="1" width="48" height="48" stroke="black" stroke-opacity="0.2" stroke-width="2"/>
                                <path d="M23 17.929L30.0711 25L23 32.0711" stroke="#BF3F27" stroke-width="2" stroke-linecap="square"/>
                                <path d="M23 17.929L30.0711 25L23 32.0711" stroke="black" stroke-opacity="0.2" stroke-width="2" stroke-linecap="square"/>
                            </svg>
                        </a>
                    </div>
                    <h3 class="text-20 mb-2.5 md:mb-[0.688rem]">Sections</h3>
                    <div class="divider border-b border-b-gray-300 mb-6 md:mb-10"></div>
                    <div class="wrapper grid grid-cols-1 md:grid-cols-2 gap-y-6 gap-x-[1.313rem] md:gap-y-[3.375rem]">
                        <a href="#" class="card v9">
                            <div class="image"><img src="./img/placeholder/narratives-thumb-1.jpg" alt="Alt Text Here" /></div>
                            <div class="details">
                                <span class="number">01</span>
                                <span class="divider block w-2.5 border-b border-b-gray-300 mb-4"></span>
                                <h4 class="title">Introduction</h4>
                            </div>
                        </a>
                        <a href="#" class="card v9 active">
                            <div class="image"><img src="./img/placeholder/narratives-thumb-2.jpg" alt="Alt Text Here" /></div>
                            <div class="details">
                                <span class="number">02</span>
                                <span class="divider block w-2.5 border-b border-b-gray-300 mb-4"></span>
                                <h4 class="title">Exploring a Foreign Place: The Lewis and Clark Expedition in Oregon Country</h4>
                            </div>
                        </a>
                        <a href="#" class="card v9">
                            <div class="image"><img src="./img/placeholder/narratives-thumb-3.jpg" alt="Alt Text Here" /></div>
                            <div class="details">
                                <span class="number">03</span>
                                <span class="divider block w-2.5 border-b border-b-gray-300 mb-4"></span>
                                <h4 class="title">Starting a New Century: The Lewis and Clark Centennial Exposition, 1905</h4>
                            </div>
                        </a>
                        <a href="#" class="card v9">
                            <div class="image"><img src="./img/placeholder/narratives-thumb-4.jpg" alt="Alt Text Here" /></div>
                            <div class="details">
                                <span class="number">04</span>
                                <span class="divider block w-2.5 border-b border-b-gray-300 mb-4"></span>
                                <h4 class="title">A Conversation</h4>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-400 pt-[1.875rem] pb-6 md:py-[3.75rem]">
        <div class="container">
            <div class=" flex flex-wrap xl:ml-[7.5rem] xl:mr-[6.438rem] gap-y-3.5 lg:gap-y-[5.8%] xl:gap-y-0 lg:gap-x-[5.8%]">
                <div class="basis-full lg:basis-[47.1%]">
                    <h3 class="text-20 mb-3 md:mb-[1.875rem]">Related Historical Records</h3>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title mb-1.5">Map of the Oregon Territory, 1841</h4>
                            <p>In 1841, Lt. Charles Wilkes, commander of the U.S. Exploring Expedition, sailed to the Pacific Northwest and began to explore the geographic region ...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-1.jpg" alt="Alt Text Here" /></div>
                    </a>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title mb-1.5">Oregon Land Donation Claim Notification</h4>
                            <p>This image shows a certificate issued March 8, 1866, that grants 640 acres of land in Clackamas County to Thomas J. Chase and his wife, Nancy...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-2.jpg" alt="Alt Text Here" /></div>
                    </a>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title mb-1.5">Territory of Oregon West of the Rocky Mountains</h4>
                            <p>Britain and America jointly occupied the Pacific Northwest from 1812 until 1846, during which time companies from each nation attempted to ...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-3.jpg" alt="Alt Text Here" /></div>
                    </a>
                </div>
                <div class="basis-full lg:basis-[47.1%]">
                    <h3 class="text-20 mb-3 md:mb-[1.875rem]">Related Oregon Encyclopedia Articles</h3>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title mb-1.5">Oregon Donation Land Law</h4>
                            <p>When Congress passed the Oregon Donation Land Law in 1850, the legislation set in motion procedures for the disposal of public lands that ...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-4.jpg" alt="Alt Text Here" /></div>
                    </a>
                </div>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-soft-black pt-[1.875rem] pb-9 md:pb-[5.688rem] md:pt-[3.75rem]">
        <div class="container">
            <h2 class="text-40 text-gold-100 mb-1">Narratives</h2>
            <div class="carousel-narratives grid grid-cols-5 lg:gap-x-4 xl:gap-x-[3.125rem] mt-6 md:mt-[2.875rem]">
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-1.jpg" alt="This Land, Oregon" /></div>
                <h3 class="title mb-0">This Land, Oregon</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-2.jpg" alt="Canneries on the Columbia" /></div>
                <h3 class="title mb-0">Canneries on the Columbia</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-3.jpg" alt="Commerce, Climate, and Community: A History of Portland and its People" /></div>
                <h3 class="title mb-0">Commerce, Climate, and Community: A History of Portland and its People</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-4.jpg" alt="igh Desert History: Southeastern Oregon" /></div>
                <h3 class="title mb-0">High Desert History: Southeastern Oregon</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-5.jpg" alt="The Oregon Coast—Forists and Green Verdent Launs" /></div>
                <h3 class="title mb-0">The Oregon Coast—"Forists and Green Verdent Launs"</h3>
            </a>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>