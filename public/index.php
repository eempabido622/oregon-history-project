<?php include "./header.html"; ?>
<section class="page-banner min-h-[18.125rem] md:min-h-[31.25rem] xl:min-h-[43.75rem]" style="background-image: url('./img/placeholder/home-banner.jpg');">
  <div class="container justify-center pt-[5.813rem] md:pt-0 md:justify-end">
    <div class="inner max-w-[44.688rem] pb-[0.938rem] md:pb-10 xl:pb-[6.875rem]">
      <h1 class="heading h2 text-white mb-5 md:mb-7">Understanding Oregon’s past through photographs, artifacts, and archival materials.</h1>
      <form class="search-form mb-2.5 md:mb-5">
        <label for="search" class="hidden">Find a record</label>
        <input type="text" name="search" id="search" placeholder="Find a record" />
        <input type="submit" value="Search" />
      </form>
      <p class="suggested mb-0">Suggested Searches   —   <span>Meriweather Lewis</span><span>Vanport Floods</span><span>Pendleton Roundup</span></p>
    </div>
  </div>
</section><!-- End of page-banner-->

<section class="bg-white py-[1.875rem] md:py-[4.875rem]">
  <div class="container">
    <div class="wrapper masonry flex flex-wrap flex-col items-center md:flex-row md:items-start md:justify-center md:gap-x-[10%] md:gap-y-[4.875rem]">
      <div class="grid-item card v4 max-w-[20.438rem] w-full mb-[1.875rem] md:mb-0 md:max-w-none md:w-[56%]">
        <a href="#" class="image auto" title="The Vanport Flood" ><img src="./img/placeholder/masonry-1.jpg" alt="The Vanport Flood" /></a>
        <div class="details">
          <h2 class="title h4 mb-1"><a href="#">Bird's-Eye View of Pendleton</a></h2>
          <p class="mb-0">Oregon Historical Society, Map 519</p>
          <span class="circle-icon bg-gold-100"><img src="./img/icons/map.svg" alt="Map" width="23.45" height="21" /></span>
        </div>
      </div>
      <div class="grid-item card v4 max-w-[15.25rem] w-full mb-[1.875rem] md:max-w-none md:w-[34%] mt-0 md:mt-[15.5rem]">
        <a href="#" class="image auto" title="The Vanport Flood" ><img src="./img/placeholder/masonry-2.jpg" alt="Meriwether Lewis to Thomas Jefferson, 1805" /></a>
        <div class="details">
          <h2 class="title h4 mb-1"><a href="#">Meriwether Lewis to Thomas Jefferson, 1805</a></h2>
          <p class="mb-0">Oregon Historical Society, Eva Emery Dye Papers Mss 1089</p>
          <span class="circle-icon bg-gold-100"><img src="./img/icons/manuscripts.svg" alt="Manuscripts" width="23.67" height="22.74" /></span>
        </div>
      </div>
      <div class="grid-item !w-[50%] hidden md:block mt-0 md:-mt-[20rem]">
        <h2 class="font-clavo font-normal text-gray-100 text-[1.75rem] mb-10">The Oregon History Project is a digital resource of the Oregon Historical Society Museum & Research Library, making hundreds of historical records and expert scholarship accessible to the public.</h2>
        <a href="#" class="btn-link">
          <span class="text mr-2.5">Explore The Records</span>
          <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
            <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
          </svg>
        </a>
      </div>
      <div class="grid-item !w-[40%] hidden md:block"></div>
      <div class="grid-item card v4  max-w-[16.25rem] w-full md:max-w-none md:!w-[40%]">
        <a href="#" class="image auto" title="The Vanport Flood" ><img src="./img/placeholder/masonry-3.jpg" alt="Captain Meriwether Lewis's Branding Iron" /></a>
        <div class="details">
          <h2 class="title h4 mb-1"><a href="#">Captain Meriwether Lewis's Branding Iron</a></h2>
          <p class="mb-0">Oregon Historical Society, OrHi 104345</p>
          <span class="circle-icon bg-gold-100"><img src="./img/icons/vase.svg" alt="Vase" width="22.92" height="22" /></span>
        </div>
      </div>
    </div>
  </div>
</section><!-- End of section-->

<section class="bg-gray-400  pt-[1.875rem] pb-11 md:py-[3.75rem]">
  <div class="container">
    <div class="head flex items-center justify-between">
      <h2 class="text-32 mb-1">Curator Articles</h2>
      <a href="#" class="btn-link hidden md:flex">
          <span class="text mr-2.5">View All</span>
          <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
            <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
          </svg>
        </a>
    </div>
    <div class="wrapper lg:max-w-[65.9%]">
      <p>Curator articles use primary documents from the Oregon Historical Society archives to help readers imagine the events, people, and issues that shaped Oregon history.</p>
    </div>
    <div class="carousel-articles grid grid-cols-3 md:gap-x-4 xl:gap-x-[3.75rem] mt-6 md:mt-[2.875rem]">
      <a href="#" class="card v1 item">
        <div class="image mb-5"><img src="./img/placeholder/curator-article-1.jpg" alt="Alt Text Here" /></div>
        <h3 class="title mb-0">The Vanport Flood</h3>
      </a>
      <a href="#" class="card v1 item">
        <div class="image mb-5"><img src="./img/placeholder/curator-article-2.jpg" alt="Alt Text Here" /></div>
        <h3 class="title mb-0">Abigail Scott Duniway's Quilt </h3>
      </a>
      <a href="#" class="card v1 item">
        <div class="image mb-5"><img src="./img/placeholder/curator-article-3.jpg" alt="Alt Text Here" /></div>
        <h3 class="title mb-0">A Look Back At Portland Jazz: When the Joint Was Jumpin'</h3>
      </a>
    </div>
  </div>
</section><!-- End of section-->

<section class="bg-soft-black pt-[1.875rem] pb-9 md:py-[3.75rem]">
  <div class="container">
    <div class="head flex items-center justify-between">
        <h2 class="text-32 text-gold-100 mb-1">Narratives</h2>
        <a href="#" class="btn-link hidden md:flex">
            <span class="text mr-2.5">View All</span>
            <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
                <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#D3B960" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </a>
    </div>
    <div class="carousel-narratives grid grid-cols-5 lg:gap-x-4 xl:gap-x-[3.125rem] mt-6 md:mt-[2.875rem]">
      <a href="#" class="card v2 item">
        <div class="image mb-5"><img src="./img/placeholder/naratives-1.jpg" alt="This Land, Oregon" /></div>
        <h3 class="title mb-0">This Land, Oregon</h3>
      </a>
      <a href="#" class="card v2 item">
        <div class="image mb-5"><img src="./img/placeholder/naratives-2.jpg" alt="Canneries on the Columbia" /></div>
        <h3 class="title mb-0">Canneries on the Columbia</h3>
      </a>
      <a href="#" class="card v2 item">
        <div class="image mb-5"><img src="./img/placeholder/naratives-3.jpg" alt="Commerce, Climate, and Community: A History of Portland and its People" /></div>
        <h3 class="title mb-0">Commerce, Climate, and Community: A History of Portland and its People</h3>
      </a>
      <a href="#" class="card v2 item">
        <div class="image mb-5"><img src="./img/placeholder/naratives-4.jpg" alt="igh Desert History: Southeastern Oregon" /></div>
        <h3 class="title mb-0">High Desert History: Southeastern Oregon</h3>
      </a>
      <a href="#" class="card v2 item">
        <div class="image mb-5"><img src="./img/placeholder/naratives-5.jpg" alt="The Oregon Coast—Forists and Green Verdent Launs" /></div>
        <h3 class="title mb-0">The Oregon Coast—"Forists and Green Verdent Launs"</h3>
      </a>
    </div>
  </div>
</section><!-- End of section-->

<section class="bg-white py-0 md:py-[5.625rem]">
  <div class="container px-0 md:px-7">

    <div class="grid grid-cols-1 md:grid-cols-2 md:gap-x-4 xl:gap-x-[3.75rem]">
      <a href="#" class="card v3 bg-gold-100">
        <div class="details">
          <h2 class="title text-32 text-soft-black mb-2">Educators</h2>
          <p class="text-soft-black">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Semper gravida arcu est sollicitudin sed. Nulla scelerisque quam tincidunt at scelerisque facilisis mi, nec arcu. Sit leo consequat fermentum sed leo fusce purus, pulvinar. Pulvinar ipsum lobortis pharetra.</p>
        </div>
        <div class="image"><img src="./img/placeholder/home-educators.jpg" alt="Educators" /></div>
      </a>
      <a href="#" class="card v3 bg-soft-black">
        <div class="details">
          <h2 class="title text-32 text-gold-100 mb-2">About Us</h2>
          <p class="text-gray-400">The Oregon History Project (OHP) is a digital resource of the Oregon Historical Society museum and research library. Hundreds of historical records and artifacts from the OHS collections are digitized, annotated, and made available to the public.</p>
        </div>
        <div class="image"><img src="./img/placeholder/home-about-us.jpg" alt="About Us" /></div>
      </a>
    </div>
  </div>
</section><!-- End of section-->
<?php include "./footer.html"; ?>