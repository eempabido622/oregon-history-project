jQuery(document).ready( function(j){
    //Toggle Menu
    j('#header .nav-toggle,#header .main-navigation .close').on('click', function(a){
        a.preventDefault();
        j('#header .main-navigation').toggleClass('menu-shown');
        j('body').toggleClass('menu-shown');
    });

    function initCarousels() {
        var checkWidth = $(window).width();
        var carousels = [ ".carousel-articles", ".carousel-narratives", ".tabs.v2", ".tabs.v3" ];

        if (checkWidth > 959) {
          
          j.each( carousels, function( i, val ) {
            var carousel = j(val);
            if (typeof carousel.data('owl.carousel') != 'undefined') {
                carousel.data('owl.carousel').destroy();
            }

            carousel.removeClass('owl-carousel owl-theme');
          });

        } else if (checkWidth < 960) {
            j('.carousel-articles,.carousel-narratives,.tabs.v2,.tabs.v3').addClass('owl-carousel owl-theme');
            //Carousels
            j('.carousel-articles').owlCarousel({
                margin: 20,
                loop: true,
                autoplay: true,
                autoWidth: true,
                items: 2,
                dots: false,
                nav: false,
                slideBy: 1,
            });
    
            j('.carousel-narratives').owlCarousel({
                margin: 20,
                loop: true,
                autoplay: true,
                autoWidth: true,
                items: 3,
                dots: false,
                nav: false,
                slideBy: 1,
                navText: ['<svg width="20" height="30" viewBox="0 0 20 30" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M18.8258 1.91821L18.3334 1.38333C17.3865 0.350445 17.0266 0 16.6478 0C16.4431 0.0183029 16.2438 0.0746517 16.0607 0.165998C15.8745 0.267323 15.7181 0.413363 15.6062 0.590219L15.4547 0.737774L15.0759 1.01444C14.5078 1.36489 14.735 1.01444 14.2994 1.58622L13.826 2.25021C13.6027 2.48356 13.3552 2.69373 13.0874 2.87732C12.8601 3.08021 12.6328 3.44909 12.3109 3.59665C11.7202 3.78726 11.1868 4.11667 10.7579 4.55576C10.468 4.88114 10.1581 5.18911 9.8299 5.47797L8.63675 6.5293L7.42468 7.80196C6.84304 8.51491 6.21015 9.18675 5.5308 9.81239C5.23509 10.0799 4.92534 10.3323 4.6028 10.5686L4.31872 10.7899L4.1104 10.9375C3.62587 11.2485 3.17531 11.6069 2.76575 12.0073L2.40591 12.3577C2.13347 12.5644 1.89133 12.8064 1.68624 13.077L1.36428 13.2984C1.01672 13.5321 0.709814 13.8186 0.455219 14.1468L0.360525 14.3313C0.360525 14.4788 -0.0182514 14.7924 0.000688553 14.9399L0.152199 15.3641C0.152199 15.5117 0.568851 15.733 0.625668 15.8621C0.73175 16.098 0.929117 16.2836 1.17489 16.3786H1.28852C1.53388 16.7096 1.84255 16.9915 2.19758 17.2086L2.44378 17.393L2.82256 17.7066C3.11087 17.9864 3.43559 18.2282 3.78844 18.4259C4.0157 18.5735 4.24297 18.721 4.45129 18.887L4.58386 19.1083C4.74189 19.3479 4.93308 19.5652 5.15203 19.7539C5.39823 19.9383 6.08002 20.4917 6.43986 20.8421C7.11582 21.6069 7.74806 22.4074 8.33374 23.2399C8.79478 23.7633 9.31583 24.2336 9.88671 24.6416C10.2844 24.9368 10.6821 25.2688 11.0609 25.6007C11.4397 25.9327 11.6101 26.1725 11.8753 26.4676L13.7692 28.1645C14.7918 28.9023 16.1176 30.1196 16.4963 29.9905C16.8754 29.7757 17.2254 29.5155 17.538 29.2158C18.9773 28.2014 18.7122 28.0354 19.678 26.5783C20.2273 25.9143 19.7727 25.8036 18.6175 24.5863C18.5006 24.4848 18.3929 24.3737 18.2955 24.2543C17.9539 23.8442 17.5363 23.5001 17.0645 23.2399L16.5721 22.6497C15.9389 21.9285 15.2555 21.2506 14.5267 20.6208C14.1248 20.2817 13.7453 19.9182 13.3904 19.5326L13.1063 19.2375C12.879 19.0161 12.6707 18.7948 12.4434 18.6103L10.7958 16.9688L9.58369 15.8621C9.58369 15.6408 8.84508 15.0875 8.63675 14.7739L9.58369 14.0177C9.87236 13.6851 10.1891 13.3766 10.5306 13.0955C10.7958 12.7635 11.1177 12.5422 11.4776 12.1733L13.1063 10.3288C13.8071 9.7866 14.4772 9.20783 15.1138 8.59507C15.5115 8.20774 15.9471 7.85729 16.3827 7.50685L17.3486 6.63997C17.8978 6.10508 18.2955 5.75464 18.5796 5.49642C19.1667 4.97997 19.8674 4.48198 20 3.89176C19.7105 3.18121 19.3148 2.51619 18.8258 1.91821Z" fill="#4EA3AA"/></svg>','']
            });
            j('.tabs.v2').owlCarousel({
                margin: 13,
                loop: false,
                autoplay: false,
                autoWidth: true,
                items: 3,
                dots: false,
                nav: false,
                slideBy: 1,
            });
            
            j('.tabs.v3').owlCarousel({
                margin: 13,
                loop: false,
                autoplay: false,
                autoWidth: true,
                items: 3,
                dots: false,
                nav: false,
                slideBy: 1,
            });
        }
    }

    //Initialize Carousels
    initCarousels();
    j(window).resize(initCarousels);	

    //Scrollbar
    j('.carousel-curator').owlCarousel({
        margin: 15,
        loop: true,
        autoplay: true,
        autoWidth: true,
        items: 3,
        dots: false,
        nav: true,
        slideBy: 1,
        navText: ['<svg xmlns="http://www.w3.org/2000/svg" width="10" height="18" viewBox="0 0 10 18" fill="none"><path d="M9 1.92896L1.92893 9.00002L9 16.0711" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>','<svg xmlns="http://www.w3.org/2000/svg" width="10" height="18" viewBox="0 0 10 18" fill="none"><path d="M1 1.92896L8.07107 9.00002L1 16.0711" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>'],
        responsive : {
            // breakpoint from 0 up
            0 : {
                items: 1,
                autoWidth: false
            },
            // breakpoint from 480 up
            640 : {
                autoWidth: true,
                items: 2,
            },
            // breakpoint from 768 up
            768 : {
                autoWidth: true,
                items: 3,
            }
        }
    });

    //Scrollbar
    const arrowHeight = 25;
    const viewportHeight = document.querySelector('.inner-main').parentNode.clientHeight;
    const contentHeight = document.querySelector('.inner-main').offsetHeight;
    const viewableRatio = viewportHeight / contentHeight; // 1/3 or 0.333333333n
    const scrollBarArea = viewportHeight - (arrowHeight * 2); // 150px
    const thumbHeight = scrollBarArea * viewableRatio;
    const dividend = contentHeight / viewportHeight;
    const pageBanner = document.querySelector('.page-banner').offsetHeight;

    j('#header .line .scrollbar').height(thumbHeight);
    
    j('#main').on('scroll', function(){
        if( dividend > 1){
            var top = document.querySelector('.inner-main').parentNode.scrollTop / dividend;
        }else{
            var top = document.querySelector('.inner-main').parentNode.scrollTop;
        }
        if( document.querySelector('.inner-main').parentNode.scrollTop > 0 ){
            j('#header').addClass('sticky');
        }else{
            j('#header').removeClass('sticky');
        }
        if( pageBanner == 0 ){
            j('#header').addClass('dark');
            j('.logo-wrap .logo').addClass('dark');
        }else{
            if( document.querySelector('.inner-main').parentNode.scrollTop > pageBanner){
                j('#header').addClass('dark');
            }else{
                j('#header').removeClass('dark');
            }
        }
        
        j('#header .line .scrollbar').css({'transform' : 'translate(0px,' + top + 'px)'});
    });

    if( dividend > 1){
        var top = document.querySelector('.inner-main').parentNode.scrollTop / dividend;
    }else{
        var top = document.querySelector('.inner-main').parentNode.scrollTop;
    }
    if( document.querySelector('.inner-main').parentNode.scrollTop > 0 ){
        j('#header').addClass('sticky');
    }else{
        j('#header').removeClass('sticky');
    }
    if( pageBanner == 0 ){
        j('#header').addClass('dark');
        j('.logo-wrap .logo').addClass('dark');
    }else{
        if( document.querySelector('.inner-main').parentNode.scrollTop > pageBanner){
            j('#header').addClass('dark');
        }else{
            j('#header').removeClass('dark');
        }
    }
    
    j('#header .line .scrollbar').css({'transform' : 'translate(0px,' + top + 'px)'});

    lightGallery(document.getElementById('lightgallery'),{
        controls: false,
        download: false,
        counter: false
    }); 

    //Tabs V3
    j('.tabs.v3').on('click', 'a', function(a){
        a.preventDefault();
        const tab = j(this);
        
        if( !tab.parent('li').hasClass('active') ){
            j('.tab-contents .tab-content.active').fadeOut('fast', function(){
                j('.tab-contents .tab-content.active').removeClass('active');
                j('.tab-contents '+tab.attr('href')+'.tab-content').fadeIn('fast').addClass('active');
                tab.closest('ul').find('li.active').removeClass('active');
                tab.parent('li').addClass('active');
            });
        }
    });
});