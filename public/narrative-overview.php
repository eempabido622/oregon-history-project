<?php include "./header.html"; ?>
<article>
    <section class="page-banner relative block lg:flex lg:flex-row min-h-0 lg:min-h-[35rem] bg-gray-100">
        <div class="container relative z-[2] justify-center pt-[4.875rem] md:pt-[9.5rem] lg:pt-0 md:justify-end">
            <div class="inner max-w-none lg:max-w-[42%] pb-[1.875rem] md:pb-10 lg:pr-[3.75rem] lg:pb-[3.75rem]">
                <span class="subhead text-gray-400 mb-2 md:mb-5">NARRATIVES</span>
                <h1 class="heading text-gray-400 h3 mb-2 md:mb-5">Commerce, Climate, and Community: A History of Portland and its People</h1>
                <span class="by text-gray-400 mb-0">by William Toll</span>
            </div>
        </div>
        <div class="background relative lg:absolute flex w-full h-[15.5rem] lg:h-full top-0 left-0 z-[1]">
            <div class="hidden lg:block text-col basis-[45%] bg-gray-100"></div>
            <div class="image-col basis-full lg:basis-[55%]"><img src="./img/placeholder/narrative-overview-banner.jpg" alt="Alt Text Here" /></div>
        </div>
    </section><!-- End of page-banner-->

    <section class="bg-gray-400 pt-8 pb-[1.875rem] md:pt-[2.813rem] md:pb-20">
        <div class="container">
            <div class="wrapper min-h-[9.25rem] relative mb-6 lg:mb-0">
                <div class="text block relative lg:absolute top-0 left-0 max-w-[25.625rem]">
                    <p>Historian William Toll examines the tensions between social classes and ethnic groups in the City of Portland and the emergence of residential patterns and government. He locates the largest city in Oregon within the broader context of urban development in the United States and the development of cities within the orbit of the Pacific Rim. Toll teaches American urban and American Jewish history at the University of Oregon.</p>
                </div>
            </div>
            <div class="wrapper flex flex-wrap items-start mr-0 xl:mr-[4.625rem] gap-x-[4.4%]">
                <div class="basis-full mb-8 hidden lg:block lg:pt-[12.5rem] lg:basis-[12.7%] lg:mb-0">
                    <div class="divider border-b-2 border-b-red-100 w-10 mb-5"></div>
                    <ul class="list-none pl-0 mb-0">
                        <li class="mb-2.5">
                            <span class="block font-proxima font-semibold uppercase text-xs">Sections —</span>
                        </li>
                        <li class="mb-2.5">
                            <a href="#" class="text-sm leading-none no-underline hover:underline hover:decoration-red-100">01<br> Introduction: Themes for an Urban History</a>
                        </li>
                        <li class="mb-2.5">
                            <a href="#" class="text-sm leading-none no-underline hover:underline hover:decoration-red-100">02<br> The Making of a Market Town</a>
                        </li>
                        <li class="mb-2.5">
                            <a href="#" class="text-sm leading-none no-underline hover:underline hover:decoration-red-100">03<br> The Mature Distribution Center</a>
                        </li>
                        <li class="mb-2.5">
                            <a href="#" class="text-sm leading-none no-underline hover:underline hover:decoration-red-100">04<br> Recovery, Energy, and War</a>
                        </li>
                        <li class="mb-0">
                            <a href="#" class="text-sm leading-none no-underline hover:underline hover:decoration-red-100">05<br> Portland Neighborhoods</a>
                        </li>
                    </ul>
                </div>
                <div class="relative basis-full lg:basis-[82.9%]">
                    <div class="card v8">
                        <div class="wrapper flex flex-wrap lg:items-end flex-col-reverse lg:flex-row mb-4 lg:mb-3.5">
                            <div class="title-wrap basis-full lg:basis-[54.4%]">
                                <span class="number">01</span>
                                <h2 class="title h3 mb-0">Introduction: Themes for an Urban History</h2>
                            </div>
                            <div class="image-wrap basis-full lg:basis-[45.6%]">
                                <div class="image mb-2"><img src="./img/placeholder/narrative-overview-image-1.jpg" alt="Alt Text Here" /></div>
                                <p class="image-caption font-proxima mb-0">Council of Jewish Women c. 1925, Call No. 360N559s.jpg</p>
                            </div>
                        </div>
                        <div class="wrapper max-w-[37.125rem] ml-6 lg:ml-[9.875rem]">
                            <p>Cities are created through charters, and state legislatures designate names, geographic boundaries, governmental form and election procedures, and powers to make contracts and tax. But students of a city’s history usually start their inquiries elsewhere. </p>
                        </div>
                    </div>
                    <div class="card v8">
                        <div class="wrapper flex flex-wrap lg:items-end flex-col-reverse lg:flex-row mb-4 lg:mb-3.5">
                            <div class="title-wrap basis-full lg:basis-[54.4%]">
                                <span class="number">02</span>
                                <h2 class="title h3 mb-0">The Making of a Market Town</h2>
                            </div>
                            <div class="image-wrap basis-full lg:basis-[45.6%]">
                                <div class="image mb-2"><img src="./img/placeholder/narrative-overview-image-2.jpg" alt="Alt Text Here" /></div>
                                <p class="image-caption font-proxima mb-0">Portland Map 1901</p>
                            </div>
                        </div>
                        <div class="wrapper max-w-[37.125rem] ml-6 lg:ml-[9.875rem]">
                            <p>The West Coast developed rapidly in the late nineteenth century through capital and supplies funneled through its port cities.</p>
                        </div>
                    </div>
                    <div class="card v8">
                        <div class="wrapper flex flex-wrap lg:items-end flex-col-reverse lg:flex-row mb-4 lg:mb-3.5">
                            <div class="title-wrap basis-full lg:basis-[54.4%]">
                                <span class="number">03</span>
                                <h2 class="title h3 mb-0">The Mature Distribution Center</h2>
                            </div>
                            <div class="image-wrap basis-full lg:basis-[45.6%]">
                                <div class="image mb-2"><img src="./img/placeholder/narrative-overview-image-3.jpg" alt="Alt Text Here" /></div>
                                <p class="image-caption font-proxima mb-0">Portland Railway Company Car, OrHi 85163</p>
                            </div>
                        </div>
                        <div class="wrapper max-w-[37.125rem] ml-6 lg:ml-[9.875rem]">
                            <p>Just as the railroad network expanded the scale of Portland’s economy, new technologies for moving people around the city, spanning rivers, and erecting multistory buildings expanded the scale of urban living. </p>
                        </div>
                    </div>
                    <div class="card v8">
                        <div class="wrapper flex flex-wrap lg:items-end flex-col-reverse lg:flex-row mb-4 lg:mb-3.5">
                            <div class="title-wrap basis-full lg:basis-[54.4%]">
                                <span class="number">04</span>
                                <h2 class="title h3 mb-0">Recovery, Energy, and War</h2>
                            </div>
                            <div class="image-wrap basis-full lg:basis-[45.6%]">
                                <div class="image mb-2"><img src="./img/placeholder/narrative-overview-image-4.jpg" alt="Alt Text Here" /></div>
                                <p class="image-caption font-proxima mb-0">Oregon Shipbuilding 1943, Or Hi 68566</p>
                            </div>
                        </div>
                        <div class="wrapper max-w-[37.125rem] ml-6 lg:ml-[9.875rem]">
                            <p>Though Portland’s population grew steadily through the late 1920s, economic opportunities slowed. Prices for timber and wheat, the mainstay of the local economy, started to decline in 1926. </p>
                        </div>
                    </div>
                    <div class="card v8">
                        <div class="wrapper flex flex-wrap lg:items-end flex-col-reverse lg:flex-row mb-3.5">
                            <div class="title-wrap basis-full lg:basis-[54.4%]">
                                <span class="number">05</span>
                                <h2 class="title h3 mb-0">Portland Neighborhoods</h2>
                            </div>
                            <div class="image-wrap basis-full lg:basis-[45.6%]">
                                <div class="image mb-2"><img src="./img/placeholder/narrative-overview-image-5.jpg" alt="Alt Text Here" /></div>
                                <p class="image-caption font-proxima mb-0">&nbsp;</p>
                            </div>
                        </div>
                        <div class="wrapper max-w-[37.125rem] ml-6 lg:ml-[9.875rem]">
                            <p>By the mid-1960s, opposition to what planners called “urban renewal”— the demolition of deteriorating mixed-use districts to make way for freeways and large public buildings—spread from very large cities like Chicago, Detroit, and Philadelphia to smaller ones like Portland. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>