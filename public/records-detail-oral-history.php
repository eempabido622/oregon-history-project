<?php include "./header.html"; ?>
<article>
    <section class="page-banner min-h-[16.25rem] md:min-h-[26.25rem] bg-gray-100">
        <div class="container justify-center pt-[5.813rem] md:pt-0 md:justify-end">
            <div class="inner max-w-[44.688rem] pb-[1.875rem] md:pb-10 xl:pb-20">
            </div>
        </div>
    </section><!-- End of page-banner-->

    <section class="bg-white pt-4 pb-[1.875rem] md:pt-[1.875rem] md:pb-20">
        <div class="container">
            <header class="record-detail-head mb-[1.438rem] lg:mb-[5.438rem]">
                <div class="left hidden lg:block basis-full lg:basis-[10.625rem] shrink-0">
                    <a href="#" class="btn-link">
                        <svg class="mr-" xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17" fill="none">
                            <path d="M9 1L1.92893 8.07107L9 15.1421" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <span class="text">Back</span>
                    </a>
                </div>
                <div class="mid basis-full mb-[2.375rem] lg:mb-0 lg:basis-[50%] relative">
                    <div class="image" id="lightgallery">
                        <a href="./img/placeholder/record-detail-oral-history.jpg" data-sub-html=".image-caption">
                            <img src="./img/placeholder/record-detail-oral-history.jpg" alt="Alt Text Here" width="413" height="auto" />
                            <span class="square-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="29" height="30" viewBox="0 0 29 30" fill="none">
                                    <circle cx="11.5485" cy="12.5251" r="7.16327" transform="rotate(-45 11.5485 12.5251)" stroke="white" stroke-width="2"/>
                                    <path d="M17.0035 18.0377L22.7506 23.6862" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M11.5 10V15" stroke="white" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                                    <path d="M9 12.5L14 12.5" stroke="white" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                                </svg>
                            </span>
                        </a>
                        <p class="image-caption" style="display: none;">Senator Maurine Neuberger's Oral History</p>
                    </div>
                </div>
                <div class="right basis-full lg:basis-[10.625rem] shrink-0">
                    <a href="#" class="btn-link alt mb-2.5">
                        <span class="text">Listen to the Audio</span>
                        <svg class="ml-1.5" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" fill="none">
                            <circle cx="15" cy="15" r="14.5" stroke="#232D33"/>
                            <path d="M20.0017 15.0579C20.0017 16.2274 19.4748 17.349 18.537 18.1759C17.5992 19.0029 16.3272 19.4675 15.0009 19.4675M15.0009 19.4675C13.6745 19.4675 12.4025 19.0029 11.4647 18.1759C10.5269 17.349 10 16.2274 10 15.0579M15.0009 19.4675V22.9901" stroke="#232D33" stroke-width="0.980769"/>
                            <path d="M14.9721 8C15.5019 8 16.01 8.18557 16.3846 8.51588C16.7592 8.8462 16.9696 9.2942 16.9696 9.76134V15.0516C16.9696 15.5193 16.7594 15.968 16.385 16.2993C16.0106 16.6306 15.5026 16.8175 14.9721 16.8192V16.8192C14.4417 16.8175 13.9336 16.6306 13.5592 16.2993C13.1848 15.968 12.9746 15.5193 12.9746 15.0516V9.76134C12.9746 9.2942 13.185 8.8462 13.5596 8.51588C13.9343 8.18557 14.4424 8 14.9721 8V8Z" stroke="#232D33" stroke-width="0.980769"/>
                        </svg>
                    </a>
                </div>
            </header>
            <div class="wrapper flex flex-wrap items-start flex-col-reverse lg:flex-row xl:ml-[7.5rem] xl:mr-[6.438rem] gap-x-[6.3%]">
                <div class="basis-full mb-1.5 lg:pr-10 lg:basis-[20.5%] lg:border-r lg:border-r-gray-300 lg:mb-0 grow-0">
                    <div class="divider block lg:hidden w-[7.5rem] mb-[1.875rem] border-b border-b-gray-300"></div>
                    <ul class="info-list">
                        <li class="mb-[1.875rem]">
                            <span class="block font-proxima font-semibold uppercase text-xs">Catalog No. —</span>
                            <span class="text-sm leading-none">OrHi 105418</span>
                        </li>
                        <li class="mb-[1.875rem]">
                            <span class="block font-proxima font-semibold uppercase text-xs">Date —</span>
                            <span class="text-sm leading-none">December 12, 1991</span>
                        </li>
                        <li class="mb-[1.875rem]">
                            <span class="block font-proxima font-semibold uppercase text-xs">Era —</span>
                            <span class="text-sm leading-none">1981-Present (Recent Oregon History)</span>
                        </li>
                        <li class="mb-[1.875rem]">
                            <span class="block font-proxima font-semibold uppercase text-xs">Record Type —</span>
                            <span class="text-sm leading-none">Oral History</span>
                        </li>
                        <li class="mb-[1.875rem]">
                            <span class="block font-proxima font-semibold uppercase text-xs">Themes —</span>
                            <span class="text-sm leading-none">Government, Law, and Politics, Women</span>
                        </li>
                        <li class="mb-[1.875rem]">
                            <span class="block font-proxima font-semibold uppercase text-xs">Credits —</span>
                            <span class="text-sm leading-none">Oregon Folklife Program</span>
                        </li>
                        <li class="mb-[1.875rem]">
                            <span class="block font-proxima font-semibold uppercase text-xs">Region —</span>
                            <span class="text-sm leading-none">None</span>
                        </li>
                        <li class="mb-0">
                            <span class="block font-proxima font-semibold uppercase text-xs">Author —</span>
                            <span class="text-sm leading-none">Clark Hansen</span>
                        </li>
                    </ul>
                </div>
                <div class="relative basis-full lg:basis-[73.2%]">
                    <h1 class="h2 mb-3">Senator Maurine Neuberger's Oral History</h1>
                    <p>This transcript is from a 1991 interview oral historian Clark Hansen conducted with Maurine Neuberger, the only woman ever elected by Oregonians to the U.S. Senate. This <a href="#">transcript selection</a> has been edited to highlight Neuberger’s views on women in politics. Hansen, of the Oregon Historical Society, visited Neuberger numerous times and spent twelve hours interviewing her about her childhood, her husband Richard Neuberger, her political career, and her opinions about women in politics. It was one of a series of interviews concerning the history of the Oregon Legislature.</p>
                    <p>Neuberger, who worked in politics between 1950 and 1966, began her career in the Oregon Legislature, where she was elected three times to the state house. She and her husband, then a state senator, were both Democrats and worked as a political team. After Richard won a seat in the U.S. Senate in 1954, she went to Washington D.C. to serve as his aide. Richard Neuberger died from a massive cerebral hemorrhage in 1960, just before his reelection campaign. Maurine Neuberger ran for his seat, becoming the third woman nationally to be elected to the U.S. Senate for a full, six-year term.</p>
                    <p>Neuberger became known for her outspokenness and independence. She took on the tobacco industry, writing a book linking smoking to cancer, entitled Smoke Screen. She also focused on consumer advocacy and environmental issues.</p>
                    <p>In interviews, Neuberger said she did not consider herself a “militant feminist.” While she contended that she was not discriminated against by her political peers, Neuberger said she thought that lack of childcare was the primary reason more women did not enter politics. Neuberger, who had no children herself, sponsored unsuccessful legislation that would have offered parents tax deductions for childcare costs.</p>
                    <p>Neuberger left the Senate in 1966. She died Feb. 22, 2000.</p>
                    <span class="post-updated support block !font-proxima mb-[1.875rem] md:mb-12">Written by Kathy Tucker, © Oregon Historical Society, 2002.</span>
                </div>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-400  pt-[1.875rem] pb-6 md:py-[3.75rem]">
        <div class="container">
            <div class="wrapper flex flex-wrap xl:ml-[7.5rem] xl:mr-[6.438rem] gap-y-3.5 lg:gap-y-[5.8%] xl:gap-y-0 lg:gap-x-[5.8%]">
                <div class="basis-full lg:basis-[47.1%]">
                    <h3 class="text-20 mb-3 md:mb-[1.875rem]">Related Oregon Encyclopedia Articles</h3>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title mb-1.5">Maurine Neuberger (1906-2000)</h4>
                            <p>Maurine Brown Neuberger entered politics as an Oregon state legislator and, as of 2010, was Oregon’s first and only woman to serve in the United States Senate.</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/records-6.jpg" alt="Alt Text Here" /></div>
                    </a>
                </div>
                <div class="hidden lg:block basis-full lg:basis-[47.1%]"></div>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>