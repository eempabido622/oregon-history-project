<?php include "./header.html"; ?>
<article>
    <section class="page-banner"></section><!-- End of page-banner-->
    <section class="bg-gray-400 pb-[1.875rem] pt-[4.625rem] md:pt-[6.25rem] xl:pt-[16.875rem] md:pb-[4.5rem]">
        <div class="container">
            <div class="image lg:max-w-[41rem] lg:mr-auto mb-6 md:mb-[1.875rem] lg:mb-12">
                <img src="./img/placeholder/authentication-image.jpg" alt="Alt Text Here" />
                <p class="image-caption text-sm font-proxima text-gray-200 mt-3 mb-0"><span class="font-semibold">Image Title.</span> Oregon Historical Society, OrgLot131_004.</p>
            </div>
            <div class="wrapper max-w-[44.688rem]">
                <h1 class="h3 mb-[0.438rem]">Authentication Required</h1>
                <p>This page requires authentication. If this is an ongoing issue, please contact <a href="#">oeinfo@ohs.org</a>.</p>
                <div class="btn-wrap mt-[1.875rem]"><a href="#" class="btn outline-gray">Log In</a></div>
            </div>
        </div>
    </section><!-- End of section-->

</article>
<?php include "./footer.html"; ?>