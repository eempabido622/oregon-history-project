<?php include "./header.html"; ?>
<article>
    <section class="page-banner"></section><!-- End of page-banner-->
    <section class="bg-gray-400 pb-[1.875rem] pt-[4.625rem] md:pt-[6.25rem] xl:pt-[9.5rem] md:pb-[3.125rem]">
        <div class="container">
            <div class="image lg:max-w-[41rem] lg:ml-auto mb-6 md:mb-[1.875rem] lg:mb-9">
                <img src="./img/placeholder/educator-guide-image.jpg" alt="Alt Text Here" />
                <p class="image-caption text-sm font-proxima text-gray-200 mt-3 mb-0"><span class="font-semibold">Image Title.</span> Oregon Historical Society, OrgLot131_004.</p>
            </div>
            <div class="wrapper max-w-[44.688rem]">
                <h1>Educator Guide</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit interdum luctus aliquet et blandit aliquet in ullamcorper. Nibh elementum, pretium, accumsan, ut mattis urna sodales amet semper. Quis adipisng neque a dolor neque tristique semper est. Gravida.</a>.</p>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-400 pt-[1.875rem] pb-11 border-t border-t-gray-100/20 md:pt-[3.125rem] md:pb-[3.375rem]">
        <div class="container">
            <div class="wrapper max-w-[44.688rem]">
                <h2 class="mb-2.5">Approaches for Using Primary Sources</h2>
                <p>Below are suggested approaches for using primary sources, including maps, photographs, manuscripts, newspapers, artifacts, ephemera, and oral histories. Teachers should use these approaches in conjunction with the documents offered in the OHP. The following approaches address several of the Oregon Common Curriculum Goals:</p>
                <ul class="tabs v3 mb-[1.875rem]">
                    <li class="active"><a href="#tab-content-maps">Maps</a></li>
                    <li><a href="#tab-content-photographs">Photographs</a></li>
                    <li><a href="#tab-content-manuscripts">Manuscripts</a></li>
                    <li><a href="#tab-content-newspapers">Newspapers</a></li>
                    <li><a href="#tab-content-artifacts">Artifacts</a></li>
                    <li><a href="#tab-content-ephemera">Ephemera</a></li>
                    <li><a href="#tab-content-oral-histories">Oral Histories</a></li>
                </ul>
                <div class="wrapper tab-contents">
                    <div id="tab-content-maps" class="wrapper tab-content active">
                        <div class="wrapper flex items-center justify-between mb-2.5">
                            <h3 class="h4 mb-0">Approach for Maps</h3>
                            <span class="circle-icon bg-gold-100"><img src="./img/icons/map.svg" alt="Map" width="23.45" height="21" /></span>
                        </div>
                        <p>Maps serve as representations of geographic, political, or cultural features. Maps are visual records of the knowledge that people value, and they point to belief systems as well as boundaries. Teachers may effectively use maps to illustrate concepts that may otherwise be difficult for students to understand, such as settlement patterns, trade routes, economic growth. and development.</p>
                        <ul>
                            <li>Have students compare and contrast old and new maps to look for transportation patterns, population centers, business districts, natural resources, parks and green spaces, and changes in natural resources. Ask the class to make predictions about future changes based on these comparisons.</li>
                            <li>Have students examine local maps/cities/areas for names of streets and cities. Ask them to consider for whom or what streets, areas, and cities are named. Have students use this information to address the question of preservation and who or what is important in local, regional, state, national, and global cultures. For example, students might consider whether names represent primarily business people, men or women, explorers, or Native Americans.</li>
                            <li>Have students use maps of exploration to learn about patterns of travel and settlement. Have them observe routes taken, geographical features, potential difficulties encountered, etc. Using background knowledge of technology available during these time periods, have students write narratives of historical fiction, describing the journey, what they viewed, and what difficulties they encountered.</li>
                            <li>Have students examine current maps with major highways and shipping routes and compare these to historical trade route maps of different time periods. They might consider why certain routes came about, how explorer routes influenced modern transportation, or what the impact of these routes have been on political or social developments.</li>
                            <li>Have students look at maps that depict population and economic centers, and describe the physical/natural features that may have contributed to the development of these areas. Students may develop ephemera that demonstrate these natural features in order to promote these areas.</li>
                        </ul>
                    </div>
                    <div id="tab-content-photographs" class="wrapper tab-content">
                        <div class="wrapper flex items-center justify-between mb-2.5">
                            <h3 class="h4 mb-0">Approach for Photographs</h3>
                            <span class="circle-icon bg-gold-100"><img src="./img/icons/map.svg" alt="Map" width="23.45" height="21" /></span>
                        </div>
                        <p>Maps serve as representations of geographic, political, or cultural features. Maps are visual records of the knowledge that people value, and they point to belief systems as well as boundaries. Teachers may effectively use maps to illustrate concepts that may otherwise be difficult for students to understand, such as settlement patterns, trade routes, economic growth. and development.</p>
                        <ul>
                            <li>Have students compare and contrast old and new maps to look for transportation patterns, population centers, business districts, natural resources, parks and green spaces, and changes in natural resources. Ask the class to make predictions about future changes based on these comparisons.</li>
                            <li>Have students examine local maps/cities/areas for names of streets and cities. Ask them to consider for whom or what streets, areas, and cities are named. Have students use this information to address the question of preservation and who or what is important in local, regional, state, national, and global cultures. For example, students might consider whether names represent primarily business people, men or women, explorers, or Native Americans.</li>
                            <li>Have students use maps of exploration to learn about patterns of travel and settlement. Have them observe routes taken, geographical features, potential difficulties encountered, etc. Using background knowledge of technology available during these time periods, have students write narratives of historical fiction, describing the journey, what they viewed, and what difficulties they encountered.</li>
                            <li>Have students examine current maps with major highways and shipping routes and compare these to historical trade route maps of different time periods. They might consider why certain routes came about, how explorer routes influenced modern transportation, or what the impact of these routes have been on political or social developments.</li>
                            <li>Have students look at maps that depict population and economic centers, and describe the physical/natural features that may have contributed to the development of these areas. Students may develop ephemera that demonstrate these natural features in order to promote these areas.</li>
                        </ul>
                    </div>
                    <div id="tab-content-manuscripts" class="wrapper tab-content">
                        <div class="wrapper flex items-center justify-between mb-2.5">
                            <h3 class="h4 mb-0">Approach for Manuscripts</h3>
                            <span class="circle-icon bg-gold-100"><img src="./img/icons/map.svg" alt="Map" width="23.45" height="21" /></span>
                        </div>
                        <p>Maps serve as representations of geographic, political, or cultural features. Maps are visual records of the knowledge that people value, and they point to belief systems as well as boundaries. Teachers may effectively use maps to illustrate concepts that may otherwise be difficult for students to understand, such as settlement patterns, trade routes, economic growth. and development.</p>
                        <ul>
                            <li>Have students compare and contrast old and new maps to look for transportation patterns, population centers, business districts, natural resources, parks and green spaces, and changes in natural resources. Ask the class to make predictions about future changes based on these comparisons.</li>
                            <li>Have students examine local maps/cities/areas for names of streets and cities. Ask them to consider for whom or what streets, areas, and cities are named. Have students use this information to address the question of preservation and who or what is important in local, regional, state, national, and global cultures. For example, students might consider whether names represent primarily business people, men or women, explorers, or Native Americans.</li>
                            <li>Have students use maps of exploration to learn about patterns of travel and settlement. Have them observe routes taken, geographical features, potential difficulties encountered, etc. Using background knowledge of technology available during these time periods, have students write narratives of historical fiction, describing the journey, what they viewed, and what difficulties they encountered.</li>
                            <li>Have students examine current maps with major highways and shipping routes and compare these to historical trade route maps of different time periods. They might consider why certain routes came about, how explorer routes influenced modern transportation, or what the impact of these routes have been on political or social developments.</li>
                            <li>Have students look at maps that depict population and economic centers, and describe the physical/natural features that may have contributed to the development of these areas. Students may develop ephemera that demonstrate these natural features in order to promote these areas.</li>
                        </ul>
                    </div>
                    <div id="tab-content-newspapers" class="wrapper tab-content">
                        <div class="wrapper flex items-center justify-between mb-2.5">
                            <h3 class="h4 mb-0">Approach for Newspapers</h3>
                            <span class="circle-icon bg-gold-100"><img src="./img/icons/map.svg" alt="Map" width="23.45" height="21" /></span>
                        </div>
                        <p>Maps serve as representations of geographic, political, or cultural features. Maps are visual records of the knowledge that people value, and they point to belief systems as well as boundaries. Teachers may effectively use maps to illustrate concepts that may otherwise be difficult for students to understand, such as settlement patterns, trade routes, economic growth. and development.</p>
                        <ul>
                            <li>Have students compare and contrast old and new maps to look for transportation patterns, population centers, business districts, natural resources, parks and green spaces, and changes in natural resources. Ask the class to make predictions about future changes based on these comparisons.</li>
                            <li>Have students examine local maps/cities/areas for names of streets and cities. Ask them to consider for whom or what streets, areas, and cities are named. Have students use this information to address the question of preservation and who or what is important in local, regional, state, national, and global cultures. For example, students might consider whether names represent primarily business people, men or women, explorers, or Native Americans.</li>
                            <li>Have students use maps of exploration to learn about patterns of travel and settlement. Have them observe routes taken, geographical features, potential difficulties encountered, etc. Using background knowledge of technology available during these time periods, have students write narratives of historical fiction, describing the journey, what they viewed, and what difficulties they encountered.</li>
                            <li>Have students examine current maps with major highways and shipping routes and compare these to historical trade route maps of different time periods. They might consider why certain routes came about, how explorer routes influenced modern transportation, or what the impact of these routes have been on political or social developments.</li>
                            <li>Have students look at maps that depict population and economic centers, and describe the physical/natural features that may have contributed to the development of these areas. Students may develop ephemera that demonstrate these natural features in order to promote these areas.</li>
                        </ul>
                    </div>
                    <div id="tab-content-artifacts" class="wrapper tab-content">
                        <div class="wrapper flex items-center justify-between mb-2.5">
                            <h3 class="h4 mb-0">Approach for Artifacts</h3>
                            <span class="circle-icon bg-gold-100"><img src="./img/icons/map.svg" alt="Map" width="23.45" height="21" /></span>
                        </div>
                        <p>Maps serve as representations of geographic, political, or cultural features. Maps are visual records of the knowledge that people value, and they point to belief systems as well as boundaries. Teachers may effectively use maps to illustrate concepts that may otherwise be difficult for students to understand, such as settlement patterns, trade routes, economic growth. and development.</p>
                        <ul>
                            <li>Have students compare and contrast old and new maps to look for transportation patterns, population centers, business districts, natural resources, parks and green spaces, and changes in natural resources. Ask the class to make predictions about future changes based on these comparisons.</li>
                            <li>Have students examine local maps/cities/areas for names of streets and cities. Ask them to consider for whom or what streets, areas, and cities are named. Have students use this information to address the question of preservation and who or what is important in local, regional, state, national, and global cultures. For example, students might consider whether names represent primarily business people, men or women, explorers, or Native Americans.</li>
                            <li>Have students use maps of exploration to learn about patterns of travel and settlement. Have them observe routes taken, geographical features, potential difficulties encountered, etc. Using background knowledge of technology available during these time periods, have students write narratives of historical fiction, describing the journey, what they viewed, and what difficulties they encountered.</li>
                            <li>Have students examine current maps with major highways and shipping routes and compare these to historical trade route maps of different time periods. They might consider why certain routes came about, how explorer routes influenced modern transportation, or what the impact of these routes have been on political or social developments.</li>
                            <li>Have students look at maps that depict population and economic centers, and describe the physical/natural features that may have contributed to the development of these areas. Students may develop ephemera that demonstrate these natural features in order to promote these areas.</li>
                        </ul>
                    </div>
                    <div id="tab-content-ephemera" class="wrapper tab-content">
                        <div class="wrapper flex items-center justify-between mb-2.5">
                            <h3 class="h4 mb-0">Approach for Ephemera</h3>
                            <span class="circle-icon bg-gold-100"><img src="./img/icons/map.svg" alt="Map" width="23.45" height="21" /></span>
                        </div>
                        <p>Maps serve as representations of geographic, political, or cultural features. Maps are visual records of the knowledge that people value, and they point to belief systems as well as boundaries. Teachers may effectively use maps to illustrate concepts that may otherwise be difficult for students to understand, such as settlement patterns, trade routes, economic growth. and development.</p>
                        <ul>
                            <li>Have students compare and contrast old and new maps to look for transportation patterns, population centers, business districts, natural resources, parks and green spaces, and changes in natural resources. Ask the class to make predictions about future changes based on these comparisons.</li>
                            <li>Have students examine local maps/cities/areas for names of streets and cities. Ask them to consider for whom or what streets, areas, and cities are named. Have students use this information to address the question of preservation and who or what is important in local, regional, state, national, and global cultures. For example, students might consider whether names represent primarily business people, men or women, explorers, or Native Americans.</li>
                            <li>Have students use maps of exploration to learn about patterns of travel and settlement. Have them observe routes taken, geographical features, potential difficulties encountered, etc. Using background knowledge of technology available during these time periods, have students write narratives of historical fiction, describing the journey, what they viewed, and what difficulties they encountered.</li>
                            <li>Have students examine current maps with major highways and shipping routes and compare these to historical trade route maps of different time periods. They might consider why certain routes came about, how explorer routes influenced modern transportation, or what the impact of these routes have been on political or social developments.</li>
                            <li>Have students look at maps that depict population and economic centers, and describe the physical/natural features that may have contributed to the development of these areas. Students may develop ephemera that demonstrate these natural features in order to promote these areas.</li>
                        </ul>
                    </div> 
                    <div id="tab-content-oral-histories" class="wrapper tab-content">
                        <div class="wrapper flex items-center justify-between mb-2.5">
                            <h3 class="h4 mb-0">Approach for Oral Histories</h3>
                            <span class="circle-icon bg-gold-100"><img src="./img/icons/map.svg" alt="Map" width="23.45" height="21" /></span>
                        </div>
                        <p>Maps serve as representations of geographic, political, or cultural features. Maps are visual records of the knowledge that people value, and they point to belief systems as well as boundaries. Teachers may effectively use maps to illustrate concepts that may otherwise be difficult for students to understand, such as settlement patterns, trade routes, economic growth. and development.</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-400 pt-[1.875rem] pb-11 border-t border-t-gray-100/20 md:pt-[3.75rem] md:pb-[4.625rem]">
        <div class="container">
            <h2 class="mb-[3.125rem]">Educator Resources</h2>
            <div class="wrapper grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-[1.875rem] xl:gap-x-[5rem] xl:gap-y-10">
                <div class="card v14">
                    <a href="#" class="image mb-6">
                        <img src="./img/placeholder/resources-image-4.jpg" alt="Alt Text Here" />
                        <span class="square-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
                                <path d="M10.0718 1H1V21H21V11.9282" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 9V1H13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 1L9 13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                        </span>
                    </a>
                    <h3 class="title h4 text-red-100 mb-0"><a href="#">Oregon Curriculum Standards</a></h3>
                </div>
                <div class="card v14">
                    <a href="#" class="image mb-6">
                        <img src="./img/placeholder/resources-image-5.jpg" alt="Alt Text Here" />
                        <span class="square-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
                                <path d="M10.0718 1H1V21H21V11.9282" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 9V1H13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 1L9 13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                        </span>
                    </a>
                    <h3 class="title h4 text-red-100 mb-0"><a href="#">Oregon Encyclopedia</a></h3>
                </div>
                <div class="card v14">
                    <a href="#" class="image mb-6">
                        <img src="./img/placeholder/resources-image-6.jpg" alt="Alt Text Here" />
                        <span class="square-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
                                <path d="M10.0718 1H1V21H21V11.9282" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 9V1H13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 1L9 13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                        </span>
                    </a>
                    <h3 class="title h4 text-red-100 mb-0"><a href="#">Online Exhibits at OHS</a></h3>
                </div>
                <div class="card v14">
                    <a href="#" class="image mb-6">
                        <img src="./img/placeholder/resources-image-6.jpg" alt="Alt Text Here" />
                        <span class="square-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
                                <path d="M10.0718 1H1V21H21V11.9282" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 9V1H13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 1L9 13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                        </span>
                    </a>
                    <h3 class="title h4 text-red-100 mb-0"><a href="#">Oregon Timeweb</a></h3>
                </div>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-600 pt-[1.875rem] pb-11 md:py-[3.75rem]">
        <div class="container">
            <div class="head flex items-center justify-between">
                <h2 class="text-32 mb-1">Curator Collections</h2>
                <a href="#" class="btn-link hidden md:flex">
                    <span class="text mr-2.5">View All</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
                        <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            </div>
            <div class="wrapper lg:max-w-[65.9%]">
            <p>Curator Collections are a series of  hand-picked dolor sit amet, consectetur adipiscing elit. Ut lorem vulputate tortor posuere netus magnis. Ullamcorper tincidunt mattis morbi porta elit nulla turpis.</p>
            </div>
            <div class="carousel-articles grid grid-cols-3 md:gap-x-4 xl:gap-x-[3.75rem] mt-6 md:mt-[2.875rem]">
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-collection-4.jpg" alt="Alt Text Here" /></div>
                <h3 class="title mb-0">Maps of Native Lands and Reservations</h3>
            </a>
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-collection-5.jpg" alt="Alt Text Here" /></div>
                <h3 class="title mb-0">Women in the Shipyards</h3>
            </a>
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-collection-6.jpg" alt="Alt Text Here" /></div>
                <h3 class="title mb-0">The Oregon Trail</h3>
            </a>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>