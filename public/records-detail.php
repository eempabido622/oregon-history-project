<?php include "./header.html"; ?>
<article>
    <section class="page-banner min-h-[16.25rem] md:min-h-[26.25rem] bg-gray-100">
        <div class="container justify-center pt-[5.813rem] md:pt-0 md:justify-end">
            <div class="inner max-w-[44.688rem] pb-[1.875rem] md:pb-10 xl:pb-20">
            </div>
        </div>
    </section><!-- End of page-banner-->

    <section class="bg-white pt-4 pb-[1.875rem] md:pt-[1.875rem] md:pb-20">
        <div class="container">
            <header class="record-detail-head mb-[1.438rem] lg:mb-[5.438rem]">
                <div class="left hidden lg:block basis-full lg:basis-[10.625rem] shrink-0">
                    <a href="#" class="btn-link">
                        <svg class="mr-" xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17" fill="none">
                            <path d="M9 1L1.92893 8.07107L9 15.1421" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                        <span class="text">Back</span>
                    </a>
                </div>
                <div class="mid basis-full mb-[2.375rem] lg:mb-0 lg:basis-[50%] relative">
                    <div class="image" id="lightgallery">
                        <a href="./img/placeholder/record-detail-image.jpg" data-sub-html=".image-caption">
                            <img src="./img/placeholder/record-detail-image.jpg" alt="Alt Text Here" width="380" height="auto" />
                            <span class="square-icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="29" height="30" viewBox="0 0 29 30" fill="none">
                                    <circle cx="11.5485" cy="12.5251" r="7.16327" transform="rotate(-45 11.5485 12.5251)" stroke="white" stroke-width="2"/>
                                    <path d="M17.0035 18.0377L22.7506 23.6862" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M11.5 10V15" stroke="white" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                                    <path d="M9 12.5L14 12.5" stroke="white" stroke-width="2" stroke-linecap="square" stroke-linejoin="round"/>
                                </svg>
                            </span>
                        </a>
                        <p class="image-caption" style="display: none;">Surveyed Portions of the Oregon Territory, 1852</p>
                    </div>
                </div>
                <div class="right basis-full lg:basis-[10.625rem] shrink-0">
                    <a href="#" class="btn-link alt mb-2.5">
                        <span class="text">View on Wayfinder</span>
                        <svg class="ml-1.5" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" fill="none">
                            <path d="M29.5 14.719C29.5 22.5631 23.0171 28.938 15 28.938C6.98292 28.938 0.5 22.5631 0.5 14.719C0.5 6.87495 6.98292 0.5 15 0.5C23.0171 0.5 29.5 6.87495 29.5 14.719Z" stroke="#232D33"/>
                            <path d="M14.9998 6.8689C13.4073 6.87219 11.8812 7.48634 10.7563 8.57657C9.63152 9.66679 8.99987 11.144 9 12.6841C9 15.4198 11.1871 17.5294 12.8174 19.5653C13.5411 20.4689 14.2685 21.3695 14.9998 22.2673C15.7307 21.3695 16.4581 20.4689 17.1821 19.5653C18.8124 17.5294 21 15.4198 21 12.6841C20.9999 11.1441 20.3681 9.66695 19.2432 8.57684C18.1183 7.48672 16.5922 6.87265 14.9998 6.86936V6.8689ZM14.9998 15.0326C14.5143 15.0326 14.0397 14.8933 13.636 14.6325C13.2324 14.3716 12.9178 14.0009 12.732 13.5671C12.5463 13.1333 12.4977 12.656 12.5924 12.1955C12.6872 11.735 12.921 11.3121 13.2643 10.9801C13.6076 10.6482 14.045 10.4221 14.5212 10.3306C14.9974 10.239 15.4909 10.2861 15.9394 10.4658C16.3879 10.6456 16.7712 10.9499 17.0409 11.3403C17.3105 11.7308 17.4544 12.1897 17.4543 12.6593C17.4543 12.971 17.3908 13.2797 17.2675 13.5677C17.1441 13.8557 16.9633 14.1173 16.7354 14.3378C16.5075 14.5582 16.2369 14.733 15.9391 14.8523C15.6413 14.9716 15.3221 15.033 14.9998 15.033V15.0326Z" stroke="#232D33"/>
                        </svg>
                    </a>
                    <a href="#" class="btn-link alt mb-2.5">
                        <span class="text">View on Timeweb</span>
                        <svg class="ml-1.5" xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" fill="none">
                            <path d="M5 14.5H25" stroke="#44535C"/><circle cx="15" cy="15" r="14.5" stroke="#44535C"/><path d="M15 11V15" stroke="#44535C"/><circle cx="15" cy="9" r="2" stroke="#44535C"/><path d="M10 18V14" stroke="#44535C"/><circle cx="10" cy="20" r="2" transform="rotate(-180 10 20)" stroke="#44535C"/><path d="M20 18V14" stroke="#44535C"/><circle cx="20" cy="20" r="2" transform="rotate(-180 20 20)" stroke="#44535C"/>
                        </svg>
                    </a>
                </div>
            </header>
            <div class="wrapper flex flex-wrap items-start flex-col-reverse lg:flex-row xl:ml-[7.5rem] xl:mr-[6.438rem] gap-x-[6.3%]">
                <div class="basis-full mb-1.5 lg:pr-10 lg:basis-[20.5%] lg:border-r lg:border-r-gray-300 lg:mb-0 grow-0">
                    <div class="divider block lg:hidden w-[7.5rem] mb-[1.875rem] border-b border-b-gray-300"></div>
                    <ul class="info-list">
                        <li>
                            <span class="block font-proxima font-semibold uppercase text-xs">Catalog No. —</span>
                            <span class="text-sm leading-none">OrHi 105418</span>
                        </li>
                        <li>
                            <span class="block font-proxima font-semibold uppercase text-xs">Date —</span>
                            <span class="text-sm leading-none">October 21, 1852</span>
                        </li>
                        <li>
                            <span class="block font-proxima font-semibold uppercase text-xs">Era —</span>
                            <span class="text-sm leading-none">1846–1880 (Treaties, Civil War, and Immigration)</span>
                        </li>
                        <li>
                            <span class="block font-proxima font-semibold uppercase text-xs">Themes —</span>
                            <span class="text-sm leading-none">Government, Law, and Politics, Oregon Trail and Resettlement, Transportation and Communication</span>
                        </li>
                        <li>
                            <span class="block font-proxima font-semibold uppercase text-xs">Credits —</span>
                            <span class="text-sm leading-none">Oregon Historical Society</span>
                        </li>
                        <li>
                            <span class="block font-proxima font-semibold uppercase text-xs">Regions —</span>
                            <span class="text-sm leading-none">Oregon Country Oregon Trail Willamette Basin</span>
                        </li>
                        <li>
                            <span class="block font-proxima font-semibold uppercase text-xs">Author —</span>
                            <span class="text-sm leading-none">John B. Preston</span>
                        </li>
                    </ul>
                </div>
                <div class="relative basis-full lg:basis-[73.2%]">
                    <h1 class="h2 mb-3">Surveyed Portions of the Oregon Territory, 1852</h1>
                    <p>The map above shows the surveyed portion of the Oregon Territory as of October 21, 1852. It was prepared by John B. Preston, first surveyor general of Oregon.</p>
                    <p>With the establishment of the Oregon Territory on August 14, 1848, all grants of claims of land in the territory were nullified. It was not until the passage of the Donation Land Act on September 27, 1850, that new provisions were made for acquiring unclaimed land, to be based upon government surveys. John B. Preston was appointed first surveyor general of Oregon, where he arrived on April 20, 1851. On June 7, 1851, Preston drove the “starting stake” for the base surveys of the territory at what is today known as the Willamette Stone. The east-west Willamette Base Line and the north-south Willamette Meridian still define surveying and legal land descriptions in Oregon and Washington state.</p>
                    <p>Preston and his crew were able to carry out their surveying and mapping to the extent depicted on this map in just over one year. Each square of the grid, termed a section, is one mile on each side and includes 640 acres of land—the maximum acreage allowed for a married couple making a claim under the Donation Land Act; a single man could claim up to 320 acres. Thirty-six sections make up a township.</p>
                    <p>The surveyors took careful note of waterways, hills, prairies, and roads. Some recognizable places are already viable communities, such as Salem and Lafayette; other communities of the period—Champoeg, Fairfield, Cincinnati, for example—have vanished as meaningful places. All three of those were river landings that were affected by floods, channel changes, and the eventual disappearance of river transportation. Notably absent from the map is Mission Bottom or Mission Landing, the by-then-abandoned site of the Methodist mission of the 1830s, and St. Paul on French Prairie. The roads that are mapped include ancient routes such as the California-Oregon Trail, which had sections that paralleled both sides of the valley, as well as newer roads established in recent years. The roads converging on Salem from the outlying farming districts, for instance, reflect its recent importance as a mill town, powered by the waters of Mill Creek.</p>
                    <span class="post-updated support block !font-proxima mb-[1.875rem] md:mb-12">This entry was last updated on March 17, 2018</span>
                    <div class="wrapper hidden lg:block">
                        <div class="divider block w-[7.5rem] mb-10 border-b border-b-gray-300"></div>
                        <h2 class="h4 mb-3">Further Reading</h2>
                        <ul class="alt list-none pl-0 max-w-[36.75rem]">
                            <li class="text-sm mb-3">Bowen, William A. <em>The Willamette Valley: Migration and Settlement on the Oregon Frontier.</em> Seattle, Wash., 1978.</li>
                            <li class="text-sm mb-3">Corning, Howard McKinley. Willamette Landings. Portland, Oreg., 2004.</li>
                            <li class="text-sm">Written by Richard Engeman, ©Oregon Historical Society, 2005.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="wrapper block lg:hidden">
                <div class="divider block w-[7.5rem] mb-[1.875rem] border-b border-b-gray-300"></div>
                <h2 class="h4 mb-3">Further Reading</h2>
                <ul class="alt list-none pl-0 max-w-[36.75rem]">
                    <li class="text-sm mb-3">Bowen, William A. <em>The Willamette Valley: Migration and Settlement on the Oregon Frontier.</em> Seattle, Wash., 1978.</li>
                    <li class="text-sm mb-3">Corning, Howard McKinley. Willamette Landings. Portland, Oreg., 2004.</li>
                    <li class="text-sm">Written by Richard Engeman, ©Oregon Historical Society, 2005.</li>
                </ul>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-400 pt-[1.875rem] pb-6 md:py-[3.75rem]">
        <div class="container">
            <div class="wrapper flex flex-wrap xl:ml-[7.5rem] xl:mr-[6.438rem] gap-y-3.5 lg:gap-y-[5.8%] xl:gap-y-0 lg:gap-x-[5.8%]">
                <div class="basis-full lg:basis-[47.1%]">
                    <h3 class="text-20 mb-3 md:mb-[1.875rem]">Related Historical Records</h3>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title mb-1.5">Map of the Oregon Territory, 1841</h4>
                            <p>In 1841, Lt. Charles Wilkes, commander of the U.S. Exploring Expedition, sailed to the Pacific Northwest and began to explore the geographic region ...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-1.jpg" alt="Alt Text Here" /></div>
                    </a>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title mb-1.5">Oregon Land Donation Claim Notification</h4>
                            <p>This image shows a certificate issued March 8, 1866, that grants 640 acres of land in Clackamas County to Thomas J. Chase and his wife, Nancy...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-2.jpg" alt="Alt Text Here" /></div>
                    </a>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title mb-1.5">Territory of Oregon West of the Rocky Mountains</h4>
                            <p>Britain and America jointly occupied the Pacific Northwest from 1812 until 1846, during which time companies from each nation attempted to ...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-3.jpg" alt="Alt Text Here" /></div>
                    </a>
                </div>
                <div class="basis-full lg:basis-[47.1%]">
                    <h3 class="text-20 mb-3 md:mb-[1.875rem]">Related Oregon Encyclopedia Articles</h3>
                    <a href="#" class="card v6 mb-2.5">
                        <div class="details">
                            <h4 class="title mb-1.5">Oregon Donation Land Law</h4>
                            <p>When Congress passed the Oregon Donation Land Law in 1850, the legislation set in motion procedures for the disposal of public lands that ...</p>
                        </div>
                        <div class="image mb-0"><img src="./img/placeholder/related-hr-4.jpg" alt="Alt Text Here" /></div>
                    </a>
                </div>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>