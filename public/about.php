<?php include "./header.html"; ?>
<article>
    <section class="page-banner"></section><!-- End of page-banner-->
    <section class="bg-gray-400 pb-[1.875rem] pt-[4.625rem] md:pt-[6.25rem] xl:pt-[9.5rem] md:pb-[3.125rem]">
        <div class="container">
            <div class="image lg:max-w-[41rem] lg:ml-auto mb-6 md:mb-[1.875rem] lg:mb-9">
                <img src="./img/placeholder/about-us-image.jpg" alt="Alt Text Here" />
                <p class="image-caption text-sm font-proxima text-gray-200 mt-3 mb-0"><span class="font-semibold">Image Title.</span> Oregon Historical Society, OrgLot131_004.</p>
            </div>
            <div class="wrapper max-w-[44.688rem]">
                <h1>About Us</h1>
                <p>The Oregon History Project (OHP) is a digital resource of the Oregon Historical Society museum and research library. Hundreds of historical records and artifacts from the unique and extensive OHS collections have been digitized, annotated, and organized according to the state of Oregon's social studies standards. This site also offers <a href="#">narratives</a> written by Pacific Northwest historians and <a href="#">resources</a> for educators. OHP is managed by Amy E. Platt who is also a contributor and editor for <a href="#">The Oregon Encyclopedia</a>.</p>
                <p>For inquiries, please <a href="#">email us</a>.</p>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-400 pt-[1.875rem] pb-11 border-t border-t-gray-100/20 md:pt-[4.375rem] md:pb-[5.563rem]">
        <div class="container">
            <h2 class="mb-6 md:mb-[3.125rem]">OHS Resources</h2>
            <div class="wrapper grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-[1.875rem] xl:gap-x-[3.438rem] xl:gap-y-[3.75rem]">
                <div class="card v14">
                    <a href="#" class="image mb-6"><img src="./img/placeholder/resources-image-1.jpg" alt="Alt Text Here" /></a>
                    <h3 class="title h4 text-red-100 mb-0"><a href="#">Research at the Library</a></h3>
                </div>
                <div class="card v14">
                    <a href="#" class="image mb-6"><img src="./img/placeholder/resources-image-2.jpg" alt="Alt Text Here" /></a>
                    <h3 class="title h4 text-red-100 mb-0"><a href="#">Top 50 Books About Oregon</a></h3>
                </div>
                <div class="card v14">
                    <a href="#" class="image mb-6"><img src="./img/placeholder/resources-image-3.jpg" alt="Alt Text Here" /></a>
                    <h3 class="title h4 text-red-100 mb-0"><a href="#">Permissions</a></h3>
                </div>
                <div class="card v14">
                    <a href="#" class="image mb-6">
                        <img src="./img/placeholder/resources-image-4.jpg" alt="Alt Text Here" />
                        <span class="square-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
                                <path d="M10.0718 1H1V21H21V11.9282" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 9V1H13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 1L9 13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                        </span>
                    </a>
                    <h3 class="title h4 text-red-100 mb-0"><a href="#">OHS Tours</a></h3>
                </div>
                <div class="card v14">
                    <a href="#" class="image mb-6">
                        <img src="./img/placeholder/resources-image-5.jpg" alt="Alt Text Here" />
                        <span class="square-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
                                <path d="M10.0718 1H1V21H21V11.9282" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 9V1H13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 1L9 13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                        </span>
                    </a>
                    <h3 class="title h4 text-red-100 mb-0"><a href="#">Traveling Trunks Program</a></h3>
                </div>
                <div class="card v14">
                    <a href="#" class="image mb-6">
                        <img src="./img/placeholder/resources-image-6.jpg" alt="Alt Text Here" />
                        <span class="square-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
                                <path d="M10.0718 1H1V21H21V11.9282" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 9V1H13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                                <path d="M21 1L9 13" stroke="white" stroke-width="2" stroke-miterlimit="10"/>
                            </svg>
                        </span>
                    </a>
                    <h3 class="title h4 text-red-100 mb-0"><a href="#">Oregon Wayfinder</a></h3>
                </div>

            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-400 pt-[1.875rem] pb-11 border-t border-t-gray-100/20 md:pt-[4.375rem] md:pb-[5.563rem]">
        <div class="container">
            <h2 class="mb-6 md:mb-[2.188rem]">Narrative Contributors</h2>
            <div class="wrapper grid grid-cols-1 lg:grid-cols-3 gap-[1.875rem] lg:gap-x-[2.5rem] lg:gap-y-[3.75rem]">
                <div class="text">
                    <h3 class="title h4 text-gray-100 mb-2">Michael McGregor</h3>
                    <p class="mb-0">Michael McGregor, an accomplished writer and Professor Emeritus of Nonfiction Writing and English at Portland State University, authored several <a href="#">narratives</a> using journals, autobiographies, letters, newspapers, photographs, and other primary documents from the Oregon Historical Society archives.</p>
                </div>
                <div class="text">
                    <h3 class="title h4 text-gray-100 mb-2">William Toll</h3>
                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sagittis neque convallis scelerisque semper purus egestas. Quam eget mi ultricies sed. Enim enim egestas ut luctus semper purus, bibendum eget fringilla. Tortor amet sed pulvinar feugiat orci viverra. Facilisi viverra.</p>
                </div>
                <div class="text">
                    <h3 class="title h4 text-gray-100 mb-2">Amy Platt</h3>
                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sagittis neque convallis scelerisque semper purus egestas. Quam eget mi ultricies sed. Enim enim egestas ut luctus semper purus, bibendum eget fringilla. Tortor amet sed pulvinar feugiat orci viverra. Facilisi viverra convallis pellentesque elementum eret.</p>
                </div>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-soft-black pt-[1.875rem] pb-9 md:pb-[5.688rem] md:pt-[3.75rem]">
        <div class="container">
            <div class="head flex items-center justify-between">
                <h2 class="text-32 text-gold-100 mb-1">Narratives</h2>
                <a href="#" class="btn-link hidden md:flex">
                    <span class="text mr-2.5">View All</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
                        <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#D3B960" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            </div>
            <div class="carousel-narratives grid grid-cols-5 lg:gap-x-4 xl:gap-x-[3.125rem] mt-6 md:mt-[2.875rem]">
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-1.jpg" alt="This Land, Oregon" /></div>
                <h3 class="title mb-0">This Land, Oregon</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-2.jpg" alt="Canneries on the Columbia" /></div>
                <h3 class="title mb-0">Canneries on the Columbia</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-3.jpg" alt="Commerce, Climate, and Community: A History of Portland and its People" /></div>
                <h3 class="title mb-0">Commerce, Climate, and Community: A History of Portland and its People</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-4.jpg" alt="igh Desert History: Southeastern Oregon" /></div>
                <h3 class="title mb-0">High Desert History: Southeastern Oregon</h3>
            </a>
            <a href="#" class="card v2 item">
                <div class="image mb-5"><img src="./img/placeholder/naratives-5.jpg" alt="The Oregon Coast—Forists and Green Verdent Launs" /></div>
                <h3 class="title mb-0">The Oregon Coast—"Forists and Green Verdent Launs"</h3>
            </a>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>