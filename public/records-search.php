<?php include "./header.html"; ?>
<article>
    <section class="page-banner min-h-0 md:min-h-[35rem]" style="background-image: url('./img/placeholder/records-search-banner.jpg');">
        <div class="container justify-center pt-[5.813rem] md:pt-0 md:justify-end">
            <div class="inner max-w-[44.688rem] pb-[1.875rem] md:pb-10 xl:pb-20">
            <h1 class="heading text-white mb-1 md:mb-4">Historical Records</h1>
            <p class="text-white mb-4 md:mb-[1.875rem]">Historical records furnish the raw data researchers use to understand the past. Interpreting multiple points of context and viewpoints help us better understand the story of Oregon.</p>
            <form class="search-form mb-0">
                <label for="search" class="hidden">Find a record</label>
                <input type="text" name="search" id="search" placeholder="Find a record" value="Oregon Territory" />
                <input type="submit" value="Search" />
            </form>
            </div>
        </div>
    </section><!-- End of page-banner-->

    <section class="bg-white pt-4 pb-[1.875rem] md:py-20">
        <div class="container">
            <div class="wrapper flex flex-wrap gap-x-[6.6%]">
                <div class="basis-full lg:basis-[26.7%] mb-8 lg:mb-0">
                    <form class="form-default">
                        <h2 class="form-heading mb-6">Refine your search</h2>
                        <div class="form-field mb-5">
                            <label for="eras" class="mb-3">Era</label>
                            <select name="eras" id="eras"><option>Select Eras…</option></select>
                        </div>
                        <div class="form-field mb-5">
                            <label for="region" class="mb-3">Region</label>
                            <select name="region" id="region"><option>Select Region…</option></select>
                        </div>
                        <div class="form-field mb-5">
                            <label for="county" class="mb-3">County</label>
                            <select name="county" id="county"><option>Select County…</option></select>
                        </div>
                        <div class="form-field mb-[2.313rem]">
                            <label for="theme" class="mb-3">Theme</label>
                            <select name="theme" id="theme"><option>Select Theme…</option></select>
                        </div>
                        <div class="divider border-b border-b-gray-100 mb-[2.313rem]"></div>
                        <h2 class="form-heading mb-[0.938rem]">Enrich your results</h2>
                        <div class="form-field flex items-start md:max-w-[16rem] mb-[0.688rem]">
                            <input type="checkbox" id="narratives" name="narratives" >
                            <label for="narratives" class="input-label ml-3.5 mb-0">Include narratives written by historians based on OHP records</label>
                        </div>
                        <div class="form-field flex items-start md:max-w-[16rem] mb-0">
                            <input type="checkbox" id="story" name="story" >
                            <label for="story" class="input-label ml-3.5 mb-0">Include stories about Oregon from the Oregon Encyclopedia</label>
                        </div>
                    </form>
                </div>
                <div class="relative basis-full lg:basis-[66.7%]">
                    <p class="results relative right-[inherit] top-[inherit] lg:absolute lg:right-0 lg:-top-[3.75rem] z-[1] font-light italic leading-none mb-2 text-right lg:mb-0 lg:text-left">126 Results</p>
                    <div class="card v5">
                        <a href="#" class="image auto" title="" ><img src="./img/placeholder/records-1.jpg" alt="Alt Text Here" /></a>
                        <div class="details">
                            <h2 class="mb-1.5"><a href="#">American Society for Encouraging the Settlement of the Oregon Territory</a></h2>
                            <span class="posted-by mb-1.5">by American Society for Encouraging the Settlement of the Oregon Territory</span>
                            <p>A persistent fox terrier named Two-Bits earned a brief measure of national fame while spending the winter of 1942–1943 at the Siskiyou Mountains' isolated Whisky Peak Lookout. Called a "war hero" by …</p>
                            <span class="tags mb-0">Oregon History Project</span>
                        </div>
                    </div>
                    <div class="card v5">
                        <a href="#" class="image auto" title="" ><img src="./img/placeholder/records-2.jpg" alt="Alt Text Here" /></a>
                        <div class="details">
                            <h2 class="mb-1.5"><a href="#">Surveyed Portions of the Oregon Territory, 1852</a></h2>
                            <span class="posted-by mb-1.5">by John B. Preston</span>
                            <p>The map above shows the surveyed portion of the Oregon Territory as of October 21, 1852. It was prepared by John B. Preston, first surveyor …</p>
                            <span class="tags mb-0">Oregon History Project</span>
                        </div>
                    </div>
                    <div class="card v5">
                        <a href="#" class="image auto" title="" ><img src="./img/placeholder/records-3.jpg" alt="Alt Text Here" /></a>
                        <div class="details">
                            <h2 class="mb-1.5"><a href="#">Map of the Oregon Territory, 1841</a></h2>
                            <span class="posted-by mb-1.5">by U.S. Exploring Expedition</span>
                            <p>In 1841, Lt. Charles Wilkes, commander of the U.S. Exploring Expedition, sailed to the Pacific Northwest and began to explore the geographic region known as …</p>
                            <span class="tags mb-0">Oregon History Project</span>
                        </div>
                    </div>
                    <div class="card v5">
                        <a href="#" class="image auto" title="" ><img src="./img/placeholder/records-4.jpg" alt="Alt Text Here" /></a>
                        <div class="details">
                            <h2 class="mb-1.5"><a href="#">Territory of Oregon West of the Rocky Mountains</a></h2>
                            <span class="posted-by mb-1.5">by U.S. Bureau of Topographical Engineers</span>
                            <p>Britain and America jointly occupied the Pacific Northwest from 1812 until 1846, during which time companies from each nation attempted to establish footholds in the …</p>
                            <span class="tags mb-0">Oregon History Project</span>
                        </div>
                    </div>
                    <div class="card v5">
                        <a href="#" class="image auto" title="" ><img src="./img/placeholder/records-5.jpg" alt="Alt Text Here" /></a>
                        <div class="details">
                            <h2 class="mb-1.5"><a href="#">A New Legal Landscape</a></h2>
                            <span class="posted-by mb-1.5">by William G. Robbins</span>
                            <p>The presence of Americans to the Oregon Country in the early 1840s led to a new legal landscape, imposed first in the Willamette Valley and …</p>
                            <span class="tags mb-0">Oregon History Project</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-wrap mt-[6.375rem] text-center">
                <a href="#" class="btn outline-gray">Learn More</a>
            </div>
        </div>
    </section><!-- End of section-->

    <section class="bg-gray-400  pt-[1.875rem] pb-11 md:py-[3.75rem]">
        <div class="container">
            <div class="head flex items-center justify-between">
                <h2 class="text-32 mb-1">Curator Articles</h2>
                <a href="#" class="btn-link hidden md:flex">
                    <span class="text mr-2.5">View All</span>
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="20" viewBox="0 0 10 20" fill="none">
                        <path d="M1.07104 2L8.14211 10.006L1.07104 18.0121" stroke="#BF3F27" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </a>
            </div>
            <div class="wrapper lg:max-w-[65.9%]">
            <p>Curator articles use primary documents from the Oregon Historical Society archives to help readers imagine the events, people, and issues that shaped Oregon history.</p>
            </div>
            <div class="carousel-articles grid grid-cols-3 md:gap-x-4 xl:gap-x-[3.75rem] mt-6 md:mt-[2.875rem]">
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-article-1.jpg" alt="Alt Text Here" /></div>
                <h3 class="title h4 mb-0">The Vanport Flood</h3>
            </a>
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-article-2.jpg" alt="Alt Text Here" /></div>
                <h3 class="title h4 mb-0">Abigail Scott Duniway's Quilt </h3>
            </a>
            <a href="#" class="card v1 item">
                <div class="image mb-5"><img src="./img/placeholder/curator-article-3.jpg" alt="Alt Text Here" /></div>
                <h3 class="title h4 mb-0">A Look Back At Portland Jazz: When the Joint Was Jumpin'</h3>
            </a>
            </div>
        </div>
    </section><!-- End of section-->
</article>
<?php include "./footer.html"; ?>